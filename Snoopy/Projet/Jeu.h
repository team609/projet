#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

#include <math.h>
#include "Niveau.h"


void jeu();
Niveau chargerPartie(std::string userName);
Niveau menu(Console* pconsole);
void afficherScores();
int conv(char caract);
void afficherMenu(Console* pConsole);
void afficherRegles(Console* pConsole);

#endif // MENU_H_INCLUDED
