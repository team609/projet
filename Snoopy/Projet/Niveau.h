#ifndef NIVEAU_H_INCLUDED
#define NIVEAU_H_INCLUDED

#ifndef MATRICE_H_INCLUDED
#define MATRICE_H_INCLUDED

#include "console.h"
#include "ActeurMob.h"
#include "Acteur.h"


void setCoordonnees(int& x, int& y);
void affichageCouleurs(char act, Console* pConsole);
void setDep(int& dx, int& dy);

class Niveau
{

private:

    ///ATTRIBUTS


    int m_score;
    int m_difficulte;
    int m_vies;
    bool m_fin;
    clock_t m_temps;
    std::string m_pseudo;

    //Matrice
    std::vector< std::vector <char> > m_tab;

    //Snoopy
    ActeurMobile m_snoopy;

    //Tous les tableaux d'acteurs :
    std::vector <Acteur> m_blocsT; //blocs pieges
    std::vector <ActeurMobile> m_blocsP; //blocs poussables
    std::vector <Acteur> m_blocsC;  //blocs cassables
    std::vector <Acteur> m_oiseaux;
    std::vector <ActeurMobile> m_balles;

public :

    Niveau(int niveau);
    Niveau(Niveau const& copie);
    //Niveau(std::vector <Acteur> blocsP, std::vector <Acteur> blocsC, std::vector <Acteur> oiseaux);
    Niveau(int niv, int score, std::string pseudo, int vies,clock_t temps, ActeurMobile snoopy, std::vector<ActeurMobile> balles, std::vector<Acteur> blocsC, std::vector<ActeurMobile> blocsP,std::vector<Acteur> blocsT,std::vector<Acteur> oiseaux,std::vector <std::vector <char> > tab);


    /*void setBlocsP(std::vector <Acteur> blocs);
    void setBlocsC(std::vector <Acteur> blocs);
    void setOiseaux(std::vector <Acteur> oiseaux);*/
    int getDifficulte() const;
    void boucle(Console* pConsole);
    void actualiserTab(Console* pconsole);
    void afficher(Console* pConsole);
    void clavier(Console* pConsole);
    void gestionCollisions(Console* pConsole, bool appuiKeyA);
    void collisionBalles();
    void gagner(Console* pConsole);
    void perdreVie(Console* pConsole);
    void perdre();
    bool collisionMortelle();
    void casserBlocC();
    void sauvegarder(Console* pConsole);
    bool verifPseudoUnique(Console* pConsole);


    bool bloquerGauche();
    bool bloquerDroite();
    bool bloquerHaut();
    bool bloquerBas();

    void libererActeur();
};

#endif // MATRICE_H_INCLUDED


#endif // NIVEAU_H_INCLUDED








/*direction : 0->gauche
              1->droite
              2->haut
              3->bas
*/
/*
void Niveau::bloquer(int direct)
{
    unsigned int i=0;

    for(i=0; i<m_blocsC.size(); i++)
    {
        if(direct==0)
            m_blocsC[i].verifGauche(m_snoopy);
        if(direct==1)
            m_blocsC[i].verifDroite(m_snoopy);
        if(direct==2)
            m_blocsC[i].verifHaut(m_snoopy);
        if(direct==3)
            m_blocsC[i].verifBas(m_snoopy);
    }

    for(i=0; i<m_blocsT.size(); i++)
    {
        if(direct==0)
            gauche=m_blocsT[i].verifGauche(m_snoopy);
        if(direct==1)
            droite=m_blocsT[i].verifDroite(m_snoopy);
        if(direct==2)
            droite=m_blocsT[i].verifHaut(m_snoopy);
        if(direct==3)
            droite=m_blocsT[i].verifBas(m_snoopy);

    }

    for(i=0; i<m_blocsP.size(); i++)
    {
        if(direct==0)
            gauche=m_blocsP[i].verifGauche(m_snoopy);
        if(direct==1)
            droite=m_blocsP[i].verifDroite(m_snoopy);
        if(direct==2)
            droite=m_blocsP[i].verifHaut(m_snoopy);
        if(direct==3)
            droite=m_blocsP[i].verifBas(m_snoopy);

    }
}
*/
