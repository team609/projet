#ifndef CONSOLE_H_INCLUDED
#define CONSOLE_H_INCLUDED

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <string>
#include <fstream>

#define X_MAX 20
#define Y_MAX 10

int alea(int inf, int sup);
float floatAlea(int inf, int sup);


enum Color
{
    BLACK = 0,
    WHITE = 15,
    RED = 12,
    GREEN = 2,
    BLUE = 9,
    YELLOW = 14,
    PURPLE = 5,
    DEFAULT = 7, // gris couleur de base de la console
};

class Console
{
    private:
        // Empecher la cr�ation
        Console();
        ~Console();

        // Empecher la copie d'objet...
        Console& operator= (const Console&){ return *this;}
        Console (const Console&){}

        // Attributs
        static Console* m_instance;

    public:

        void _setColor(int front, int back);
        // M�thodes statiques (publiques)
        static Console* getInstance();
        static void deleteInstance();

        // M�thodes publiques
        void gotoLigCol(int lig, int col);
        bool isKeyboardPressed();
        char getInputKey();
        void setColor(Color col);
        void clearConsole();
        void pause();
        void ShowConsoleCursor(bool showFlag);
};



#endif // CONSOLE_H_INCLUDED
