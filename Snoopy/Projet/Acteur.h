#ifndef ACTEUR_H_INCLUDED
#define ACTEUR_H_INCLUDED

#include "console.h"


class Acteur
{

//modifi�/lu par acteur_mobile
protected:
    int m_x, m_y;
    int m_couleur;
    char m_type;
    bool m_alive;

public:
    //Constructeurs-Destructeurs
    Acteur();
    Acteur(Acteur const& copie);
    Acteur(char type, int x, int y);
    ~Acteur();

    //setters
    void setCoord(int x, int y);
    void setX(int x);
    void setY(int y);
    void setAlive(bool alive);
    void setType(char type);

    //Getters
    int getX() const;
    int getY() const;
    bool vivant() const;
    char type() const;

    //Methodes
    void envoiTab(std::vector<std::vector<char> >& tab);
    void effacer(std::vector< std::vector <char> >& tab);
    bool verifGauche(Acteur const& snoopy);
    bool verifDroite(Acteur const& snoopy);
    bool verifHaut(Acteur const& snoopy);
    bool verifBas(Acteur const& snoopy);

};
#endif // ACTEUR_H_INCLUDED

/*

    bool getLife() const;
    int get_x() const;
    int get_y() const;
    char getType() const;

    */
