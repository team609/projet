#include "Niveau.h"
#include "Jeu.h"
#include "Acteur.h"
#include "windows.h"

int Niveau::getDifficulte() const
{
    return m_difficulte;
}

Niveau::Niveau(Niveau const& copie)
{
    m_difficulte=copie.m_difficulte;
    m_score=copie.m_score;
    m_pseudo=copie.m_pseudo;
    m_vies=copie.m_vies;
    m_temps=copie.m_temps;
    m_fin=copie.m_fin;

    m_tab=copie.m_tab;
    m_snoopy.setX(copie.m_snoopy.getX());
    m_snoopy.setY(copie.m_snoopy.getY());
    m_snoopy.setType('S');
    m_balles=copie.m_balles;
    m_blocsC=copie.m_blocsC;
    m_blocsP=copie.m_blocsP;
    m_blocsT=copie.m_blocsT;
    m_oiseaux=copie.m_oiseaux;

}
Niveau::Niveau(int niv, int score, std::string pseudo, int vies,clock_t temps, ActeurMobile snoopy, std::vector<ActeurMobile> balles, std::vector<Acteur> blocsC, std::vector<ActeurMobile> blocsP,std::vector<Acteur> blocsT,std::vector<Acteur> oiseaux,std::vector <std::vector <char> > tab)
{
    m_difficulte=niv;
    m_score=score;
    m_pseudo=pseudo;
    m_vies=vies;
    m_temps=temps;
    m_fin=false;

    m_tab=tab;
    m_snoopy.setX(snoopy.getX());
    m_snoopy.setY(snoopy.getY());
    m_snoopy.setType('S');
    m_balles=balles;
    m_blocsC=blocsC;
    m_blocsP=blocsP;
    m_blocsT=blocsT;
    m_oiseaux=oiseaux;

}

bool Niveau::bloquerGauche()
{
    unsigned int i=0;

    for(i=0; i<m_blocsC.size(); i++)
    {
        if(m_blocsC[i].verifGauche(m_snoopy))
            return true;
    }

    for(i=0; i<m_blocsP.size(); i++)
    {
        if(m_blocsP[i].verifGauche(m_snoopy))
        {
            if(m_blocsP[i].getPeutBouger()) //Verifie si le bloc a deja boug�
            {
                if(m_blocsP[i].blocPeutBouger(1, m_tab)) //Verifie si le bloc est coinc�
                {
                    m_blocsP[i].setPeutBouger(false); //Changer le booleen
                    m_blocsP[i].setDepX(-1); //Initier le d�placement
                    return false;
                }
                return true;
            }
            return true;
        }
    }

    return false;
}

bool Niveau::bloquerDroite()
{
    unsigned int i=0;

    for(i=0; i<m_blocsC.size(); i++)
    {
        if(m_blocsC[i].verifDroite(m_snoopy))
            return true;
    }
    for(i=0; i<m_blocsP.size(); i++)
    {
        if(m_blocsP[i].verifDroite(m_snoopy))
        {
            if(m_blocsP[i].getPeutBouger())
            {
                if(m_blocsP[i].blocPeutBouger(2, m_tab))
                {
                    m_blocsP[i].setPeutBouger(false);
                    m_blocsP[i].setDepX(1);
                    return false;
                }
                return true;
            }
            return true;
        }
    }

    return false;
}


bool Niveau::bloquerHaut()
{
    unsigned int i=0;

    for(i=0; i<m_blocsC.size(); i++)
    {
        if(m_blocsC[i].verifHaut(m_snoopy))
            return true;
    }
    for(i=0; i<m_blocsP.size(); i++)
    {
        if(m_blocsP[i].verifHaut(m_snoopy))
        {
            if(m_blocsP[i].getPeutBouger())
            {
                if(m_blocsP[i].blocPeutBouger(3, m_tab))
                {
                    m_blocsP[i].setPeutBouger(false);
                    m_blocsP[i].setDepY(-1);
                    return false;
                }
                return true;
            }
            return true;
        }
    }

    return false;
}


bool Niveau::bloquerBas()
{
    unsigned int i=0;

    for(i=0; i<m_blocsC.size(); i++)
    {
        if(m_blocsC[i].verifBas(m_snoopy))
            return true;

    }
    for(i=0; i<m_blocsP.size(); i++)
    {
        if(m_blocsP[i].verifBas(m_snoopy))
        {
            if(m_blocsP[i].getPeutBouger())
            {
                if(m_blocsP[i].blocPeutBouger(4, m_tab))
                {
                    m_blocsP[i].setPeutBouger(false);
                    m_blocsP[i].setDepY(1);
                    return false;
                }
                return true;
            }
            return true;
        }
    }

    return false;
}

///
void Niveau::gestionCollisions(Console* pConsole, bool appuiKeyA)
{
    unsigned int i=0;

    ///Snoopy "mange" un oiseau
    for(i=0; i<m_oiseaux.size(); i++)
    {
        if(m_snoopy.collision(m_oiseaux[i]))
        {
            m_oiseaux[i].setAlive(false);
            m_score+=1000;
        }
    }

    ///Snoopy casse un bloc
    for(i=0; i<m_blocsC.size(); i++)
    {
        if(m_blocsC[i].verifGauche(m_snoopy) || m_blocsC[i].verifDroite(m_snoopy) || m_blocsC[i].verifHaut(m_snoopy) || m_blocsC[i].verifBas(m_snoopy))

        {
            if(appuiKeyA)
            {
                m_blocsC[i].setAlive(false);
                m_score+=25;
            }
        }
    }
}


Niveau::Niveau(int niveau)
    :m_difficulte(niveau)
{
    int x=0, y=0, i=0;

    ///INIT TAB
    for(i=0; i<X_MAX; i++)
    {
        m_tab.push_back(std::vector<char> (Y_MAX, ' '));
    }

    ///INIT SNOOPY
    m_snoopy.setType('S');
    m_snoopy.setCoord(X_MAX/2,Y_MAX/2);
    m_snoopy.setDepX(0);
    m_snoopy.setDepY(0);

    ///INIT OISEAUX
    Acteur oiseauTemp('O',0,0);
    m_oiseaux.push_back(oiseauTemp);
    oiseauTemp.setCoord(0,Y_MAX-1);
    m_oiseaux.push_back(oiseauTemp);
    oiseauTemp.setCoord(X_MAX-1,0);
    m_oiseaux.push_back(oiseauTemp);
    oiseauTemp.setCoord(X_MAX-1,Y_MAX-1);
    m_oiseaux.push_back(oiseauTemp);


    ///INIT BLOCS type P
    int nbTemp=alea(2,10);
    int dx=0, dy=0;
    for(i=0; i<nbTemp; i++)
    {
        setCoordonnees(x,y);
        dx=0;
        dy=0;
        ActeurMobile blocTemp('P',x,y,dx,dy);
        blocTemp.setPeutBouger(true);
        m_blocsP.push_back(blocTemp);
    }

    ///INIT BLOCS type C
    nbTemp=alea(2,15);
    for(i=0; i<nbTemp; i++)
    {
        setCoordonnees(x,y);
        Acteur blocTemp('C',x,y);
        m_blocsC.push_back(blocTemp);
    }

    ///INIT BLOCS type T
    nbTemp=alea(1,5*m_difficulte);
    for(i=0; i<nbTemp; i++)
    {
        setCoordonnees(x,y);
        Acteur blocTemp('T',x,y);
        m_blocsT.push_back(blocTemp);
    }

    ///INIT balles
    nbTemp=alea(1,m_difficulte);
    for(i=0; i<nbTemp; i++)
    {
        //setCoordonnees(x,y);
        x=X_MAX/2;
        y=Y_MAX/2+1;
        setDep(dx,dy);
        ActeurMobile balleTemp('B',x,y,dx,dy);
        m_balles.push_back(balleTemp);
    }

    m_vies=3;
    m_fin=false;
    m_pseudo="";
    m_temps=time(NULL);
    m_score=0;
}

void Niveau::boucle(Console* pConsole)
{
    while(!m_fin)
    {
        clavier(pConsole);
        collisionBalles();
        actualiserTab(pConsole);
        libererActeur();
        afficher(pConsole);
        gagner(pConsole);
        perdreVie(pConsole);
        perdre();


    }
    pConsole->pause();

}

/*void Niveau::setBlocsC(std::vector <Acteur> blocs)
{
    m_blocsC=blocs;
}

void Niveau::setBlocsP(std::vector <Acteur> blocs)
{
    m_blocsP=blocs;
}*/

/*void Niveau::setOiseaux(std::vector <Acteur> oiseaux)
{
    m_oiseaux=oiseaux;
}*/



void Niveau::afficher(Console* pConsole)
{
    int i=0, j=0;

    pConsole->ShowConsoleCursor(false);

    pConsole->gotoLigCol(45,0);
    std::cout<<"Temps restant : "<<60-m_temps;
    pConsole->gotoLigCol(45,3);
    std::cout<<"Vies restantes : "<<m_vies;
    pConsole->gotoLigCol(45,5);
    std::cout<<"score : "<<m_score;

    pConsole->gotoLigCol(0,0);

    for(i=0; i<Y_MAX; i++)
    {
        for(j=0; j< X_MAX; j++)
        {
            switch(m_tab[j][i])
            {
            case 'S' :
                pConsole->_setColor(WHITE,BLACK);
                break;

            case 'B' :
                pConsole->_setColor(RED,BLACK);
                break;

            case 'C' :
                pConsole->_setColor(BLUE,BLACK);
                break;

            case 'P' :
                pConsole->_setColor(GREEN,BLACK);
                break;

            case 'T' :
                pConsole->_setColor(PURPLE,BLACK);
                break;

            case 'O' :
                pConsole->_setColor(YELLOW,BLACK);
                break;

            default:
                break;
            }
        std::cout<<m_tab[j][i];
        //affichageCouleurs(m_tab[j][i], pConsole);
        pConsole->_setColor(WHITE,BLACK);
        std::cout << " ";
    }
    std::cout<<std::endl<<std::endl;
}
}

void setCoordonnees(int& x, int& y)
{
    do
    {
        //x et y tir�s au hasard sans d�passer
        x=alea(0,X_MAX-1);
        y=alea(0,Y_MAX-1);
    }
    while((x==0&&y==0) || (x==0&&y==Y_MAX-1) || (x==X_MAX-1&&y==0) || (x==X_MAX-1&&y==Y_MAX-1) || (x==(X_MAX)/2&&y==(Y_MAX)/2));
    //coordonn�es differentes de celles des oiseaux/snoopy au debut
}

void setDep(int& dx, int& dy)
{
    dx=alea(1,1);
    int test=alea(0,1);
    if(test==0)
        dy=dx;
    if(test==1)
        dy=-dx;
}



void Niveau::clavier(Console* pConsole)
{
    bool appuiKeyA=false;


    if(pConsole->isKeyboardPressed())
    {
        char key=pConsole->getInputKey();

        switch(key)
        {
        case 'j' :
            //case 75:
            if(!bloquerGauche())
            {
                m_snoopy.setDepX(-1);
                m_snoopy.setDepY(0);
            }
            break;

        case 'l' :
            //case 77:
            if(!bloquerDroite())
            {
                m_snoopy.setDepX(1);
                m_snoopy.setDepY(0);
            }
            break;

        case 'i' :
            //case 72:
            if(!bloquerHaut())
            {
                m_snoopy.setDepY(-1);
                m_snoopy.setDepX(0);
            }
            break;

        case 'k' :
            //case 80:
            if(!bloquerBas())
            {
                m_snoopy.setDepY(1);
                m_snoopy.setDepX(0);
            }
            break;

        case 'a' :
            appuiKeyA=true;
            break;

        case 's' :
            sauvegarder(pConsole);
            ///retour menu
            break;

        case 'q' :
            m_fin=true;
            break;

        default:
            break;
        }

        gestionCollisions(pConsole,appuiKeyA);

    }
}

void Niveau::gagner(Console* pConsole)
{
    if(m_oiseaux.size()==0)
    {
        m_fin=true;
        pConsole->clearConsole();
        pConsole->gotoLigCol(0,5);
        m_score+=(60-m_temps)*100;
        std::cout << "You finished level " << m_difficulte << " with a score of " << m_score << "! Well Done!!" << std::endl;
        if(m_difficulte==3)
        {
            std::cout<<"You won! "<<std::endl<<"Total score : "<<m_score<<std::endl;
        }
    }
}

void Niveau::actualiserTab(Console* pconsole)
{
    unsigned int i=0;

    m_temps=(double)clock()/CLOCKS_PER_SEC;

    m_snoopy.deplacement();

    for(i=0; i<m_blocsP.size(); i++)
    {
        m_blocsP[i].deplacement();
    }

    clavier(pconsole);

    //pconsole->gotoLigCol(45,10);
    //std::cout<<"dy= "<<m_snoopy.getDepY();

    for(i=0; i<m_oiseaux.size(); i++)
    {
        m_oiseaux[i].envoiTab(m_tab);
        m_oiseaux[i].effacer(m_tab);
    }

    for(i=0; i<m_blocsC.size(); i++)
    {
        m_blocsC[i].envoiTab(m_tab);
        m_blocsC[i].effacer(m_tab);
    }
    for(i=0; i<m_blocsP.size(); i++)
    {
        m_blocsP[i].effacer(m_tab);
        m_blocsP[i].envoiTab(m_tab);

        m_blocsP[i].stop();
    }
    for(i=0; i<m_blocsT.size(); i++)
    {
        m_blocsT[i].envoiTab(m_tab);
        m_blocsT[i].effacer(m_tab);
    }
    for(i=0; i<m_balles.size(); i++)
    {
        static int j=0;
        j++;
        ///Gestion balles 1 fois tous les x tours de boucle///
        if(j%2==0)
        {
            m_balles[i].deplacement();
            m_balles[i].envoiTab(m_tab);
            m_balles[i].effaceTab(m_tab);
        }
    }

    m_snoopy.effaceTab(m_tab);
    m_snoopy.envoiTab(m_tab);
    m_snoopy.stop();

}

bool Niveau::collisionMortelle()
{
    unsigned int i=0;

    for(i=0; i< m_balles.size(); i++)
    {
        if(m_snoopy.collision(m_balles[i]))
            return true;
    }
    for(i=0; i<m_blocsT.size(); i++)
    {
        if(m_snoopy.collision(m_blocsT[i]))
            return true;
    }

    return false;

}

void Niveau::perdreVie(Console* pConsole)
{
    if(collisionMortelle())
    {
        pConsole->clearConsole();
        pConsole->gotoLigCol(15,16);
        system("color 40");
        std::cout<<"You loose! ";
        Sleep(100);
        system("color F");

        m_vies--;
    }
    else if(60-m_temps<=0)
    {
        pConsole->clearConsole();
        pConsole->gotoLigCol(15,15);
        std::cout<<"Temps ecoule !";
        m_vies--;
    }
}

void Niveau::perdre()
{
    if(m_vies<=0)
        m_fin=true;


}

void Niveau::libererActeur()
{
    unsigned int i=0;

    for(i=0; i<m_oiseaux.size(); i++)
    {
        if(!m_oiseaux[i].vivant())
            m_oiseaux.erase(m_oiseaux.begin()+i);
    }

    for(i=0; i<m_blocsC.size(); i++)
    {
        if(!m_blocsC[i].vivant())
            m_blocsC.erase(m_blocsC.begin()+i);
    }

    for(i=0; i<m_blocsP.size(); i++)
    {
        if(!m_blocsP[i].vivant())
            m_blocsP.erase(m_blocsP.begin()+i);
    }

    for(i=0; i<m_blocsT.size(); i++)
    {
        if(!m_blocsC[i].vivant())
            m_blocsT.erase(m_blocsT.begin()+i);
    }


}

void Niveau::sauvegarder(Console* pConsole)
{
    unsigned int i=0;
    std::string fileName("fichiers/");
    while(!verifPseudoUnique(pConsole)) {}
    fileName+=m_pseudo;
    fileName+=".txt";
    ///std::ifstream file(fileName.c_str());
    std::ofstream fichier(fileName.c_str(), std::ios::out | std::ios::trunc);
    if(fichier)
    {
        fichier << 'S' << ' ' << m_snoopy.getX() << ' ' << m_snoopy.getY() << std::endl;

        for (i=0; i<m_balles.size(); i++)
        {
            fichier << 'B' << ' ' << m_balles[i].getX() << ' ' << m_balles[i].getY() << ' ' << m_balles[i].getDepX() << ' ' << m_balles[i].getDepY() << std::endl;
        }

        for (i=0; i<m_oiseaux.size(); i++)
        {
            fichier << 'O' << ' ' << m_oiseaux[i].getX() << ' ' << m_oiseaux[i].getY() << std::endl;
        }

        for (i=0; i<m_blocsT.size(); i++)
        {
            fichier << 'T' << ' ' << m_blocsT[i].getX() << ' ' << m_blocsT[i].getY() << std::endl;
        }

        for (i=0; i<m_blocsP.size(); i++)
        {
            fichier << 'P' << ' ' << m_blocsP[i].getX() << ' ' << m_blocsP[i].getY()/* << ' ' << m_blocsP[i].getbooldep()*/ << std::endl;
        }

        for (i=0; i<m_blocsC.size(); i++)
        {
            fichier << 'C' << ' ' << m_blocsC[i].getX() << ' ' << m_blocsC[i].getY() << std::endl;
        }

        fichier << "N " << m_difficulte << std::endl;
        fichier << "t " << (60-m_temps) << std::endl;
        fichier << "V " << m_vies << std::endl;
        fichier << "s " << m_score << std::endl;

        fichier.close();
    }

//    i=menu(pConsole);
}

bool Niveau::verifPseudoUnique(Console* pConsole)
{
    bool pseudoUnique=false;
    system("cls");
    pConsole->gotoLigCol(20,5);
    std::cout << "Entrez un pseudo pour sauvegarder votre partie" << std::endl;
    while(!pseudoUnique)
    {
        std::cin >> m_pseudo;
        std::string fileName("fichiers/");
        fileName+=m_pseudo;
        fileName+=".txt";
        std::ifstream file(fileName.c_str());
        if(file)
        {
            file.close();
            std::cout << "Ce pseudo est deja pris, choisissez-en un nouveau" << std::endl;
        }
        else
        {
            return true;
        }
    }
    return false;
}


void Niveau::collisionBalles()
{
    unsigned int i, c, p, t;
    bool dirXChange=false, dirYChange=false;
    for(i=0; i<m_balles.size(); i++)
    {
        for(c=0; c<m_blocsC.size(); c++)
        {
            if((m_balles[i].getX()+m_balles[i].getDepX()==m_blocsC[c].getX())&&((m_balles[i].getY()+m_balles[i].getDepY())==m_blocsC[c].getY()))
            {
                switch(rand()%2)
                {
                case 0:
                    m_balles[i].setDepY(((-1)*(m_balles[i].getDepY())));
                    dirYChange=true;
                    break;
                case 1:
                    m_balles[i].setDepX(-m_balles[i].getDepX());
                    dirXChange=true;
                    break;
                }
            }
        }

        for(p=0; p<m_blocsP.size(); p++)
        {
            if((m_balles[i].getX()+m_balles[i].getDepX()==m_blocsP[p].getX())&&((m_balles[i].getY()+m_balles[i].getDepY())==m_blocsP[p].getY()))
            {
                switch(rand()%2)
                {
                case 0:
                    m_balles[i].setDepY(((-1)*(m_balles[i].getDepY())));
                    if(dirYChange)
                    {
                        m_balles[i].setDepX(-m_balles[i].getDepX());
                    }
                    else
                    {
                        dirYChange=true;
                    }
                    break;
                case 1:
                    m_balles[i].setDepX(-m_balles[i].getDepX());
                    if(dirXChange)
                    {
                        m_balles[i].setDepY(-m_balles[i].getDepY());
                    }
                    else
                    {
                        dirXChange=true;
                    }
                    break;
                }
            }
        }

        for(t=0; t<m_blocsT.size(); t++)
        {
            if((m_balles[i].getX()+m_balles[i].getDepX()==m_blocsT[t].getX())&&((m_balles[i].getY()+m_balles[i].getDepY())==m_blocsT[t].getY()))
            {
                switch(rand()%2)
                {
                case 0:
                    m_balles[i].setDepY(((-1)*(m_balles[i].getDepY())));
                    if(dirYChange)
                    {
                        m_balles[i].setDepX(-m_balles[i].getDepX());
                    }
                    else
                    {
                        dirYChange=true;
                    }
                    break;
                case 1:
                    m_balles[i].setDepX(-m_balles[i].getDepX());
                    if(dirXChange)
                    {
                        m_balles[i].setDepY(-m_balles[i].getDepY());
                    }
                    else
                    {
                        dirXChange=true;
                    }
                    break;
                }
            }
        }
    }
}
