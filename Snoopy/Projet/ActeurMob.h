#ifndef ACTEUR_MOBILE_H_INCLUDED
#define ACTEUR_MOBILE_H_INCLUDED

#include <iostream>
#include "Acteur.h"

class ActeurMobile : public Acteur
{
private:

    int m_dx, m_dy;
    int m_mx, m_my;
    bool m_peutBouger;


public:
    ActeurMobile();
    ActeurMobile(char type, int x, int y, int dx=0, int dy=0);
    ~ActeurMobile();

    void setDepX(int dx);
    void setDepY(int dy);

    int getDepX();
    int getDepY();

    void setPeutBouger(bool peuBouger);
    bool getPeutBouger();

    void stop();
    void deplacement();
    void effaceTab(std::vector< std::vector <char> >& tab);

    ///Si le bloc P � bouger n'est pas bloqu�
    bool blocPeutBouger(int direction, std::vector< std::vector <char> >& tab);

    ///si l'acteur est en coolision avec la cible
    bool collision(Acteur const& cible);
};


#endif // ACTEUR_MOBILE_H_INCLUDED
