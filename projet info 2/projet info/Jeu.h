#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

#include <math.h>
#include "console.h"
#include "Niveau.h"

//appelle les bonnes fonctions en fonction du choix du joueur
void menu(Console* pconsole);

//lit et affiche le fichier txt avec les scores
void afficherScores();

//lit le fichier txt et retourne le Niveau charg�
Niveau chargerPartie(std::string userName, int& niv);

//convertit un char en int
int conv(char caract);

//A base de cout affiche le beau Snoopy
void affichageMenu(Console* pConsole);

//Affiche les regles
void regles(Console* pConsole);


#endif // MENU_H_INCLUDED
