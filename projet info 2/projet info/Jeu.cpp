#include <string>
#include <fstream>
#include <sstream>

#include "Jeu.h"
#include "partie.h"


void menu(Console* pConsole)
{
    int choix=0, niveau=0, lig=16;
    bool quit=false;
    std::string mdp, userName;

    system("mode con LINES=35 COLS=110");

    Niveau n1(chargerPartie("niveau1",niveau)), n2(chargerPartie("niveau2", niveau)), n3(chargerPartie("niveau3", niveau));

    Partie partie(n1,n2,n3);

    choix=0;

    pConsole->clearConsole();
    while (!quit)
    {
        //on affiche le curseur pour faire le choix
        pConsole->ShowConsoleCursor(true);

        partie.setFin(false);

        choix=0;
        pConsole->clearConsole();

        affichageMenu(pConsole);

        ///tant que le choix n'est pas effectu�
        while(!choix)
        {
            //le curseur se deplace � la bonne ligne
            pConsole->gotoLigCol(1,lig);

            //on prend le choix de l'utilisateur avec les commandes 'i' et 'k'
            if(pConsole->isKeyboardPressed())
            {
                char key=pConsole->getInputKey();
                switch(key)
                {
                case 'i': //si le curseur n'est pas trop haut
                    if(lig>16)
                    {
                        lig-=2;
                    }
                    break;

                case 'k': //si le curseur n'est pas trop bas
                    if(lig<26)
                    {
                        lig+=2;
                    }
                    break;

                case ' ': //on selectionne le choix
                    choix=(lig-14)/2;
                    break;

                }
            }
            //choix=saisie_int(choix,1,6);

            ///EN FONCTION DU CHOIX
            switch(choix)
            {

            //on lance le niveau 1
            case 1:
                pConsole->clearConsole();
                partie.setNiveau(1);

                partie.lancerPartie(pConsole);
                break;


            //charger partie
            case 2:
                pConsole->clearConsole();
                std::cout << "Entrez votre pseudo" << std::endl;
                std::getline(std::cin,userName);

                ///on recupere le niveau a travers le fichier texte
                partie.setLvl(chargerPartie(userName,niveau));

                std::cout<<"         "<<niveau;
                pConsole->pause();

                //si on trouve pas le fichier txt on renvoie le niveau 0

                //on lance la partie au niveau trouve
                if(niveau!=0)
                {
                    partie.setCharge(true);
                    partie.lancerPartie(pConsole);
                }
                //si on trouve pas on retourne au menu
                else
                {
                    pConsole->clearConsole();
                    std::cout<<"Pseudo inconnu !";

                }
                break;


            //acceder a un niveau par mot de passe
            case 3:
                pConsole->clearConsole();
                std::cout<< "Entrez un Mot de Passe : (Pour revenir au menu principal, entrez \"menu\")" << std::endl << std::endl << std::endl;
                std::cout << "                               ";
                std::getline(std::cin, mdp);

                /// Selon les correspondances du mot de passe entr�, on retourne le numero du niveau appropri�

                if(mdp=="a") //niv 1
                {
                    pConsole->clearConsole();
                    partie.setNiveau(1);
                    partie.lancerPartie(pConsole);
                }
                else if(mdp=="b") //niv 2
                {
                    pConsole->clearConsole();
                    partie.setNiveau(2);
                    partie.lancerPartie(pConsole);
                }
                else if(mdp=="c") //niv 3
                {
                    pConsole->clearConsole();
                    partie.setNiveau(3);
                    partie.lancerPartie(pConsole);
                }
                else if(mdp=="menu")
                {
                    pConsole->clearConsole();
                    pConsole->gotoLigCol(5,5);
                    std::cout << "Vous allez etre redirige vers le menu" << std::endl << std::endl;
                    pConsole->pause();
                }
                else
                {
                    pConsole->clearConsole();
                    pConsole->gotoLigCol(5,5);
                    std::cout << "Desole, mauvais mot de passe, vous allez etre redirige vers le menu" << std::endl << std::endl;
                    pConsole->pause();
                }

                break;



            //on affiche les regles
            case 4:
                pConsole->clearConsole();
                regles(pConsole);
                pConsole->pause();
                break;


            //on affiche les scores
            case 5:
                pConsole->clearConsole();
                afficherScores();
                pConsole->pause();
                break;


            //on quitte le menu (le jeu)
            case 6:

                quit=true;
                break;
            }
        }
    }
}


void afficherScores()
{
    int rang=1;
    std::ifstream fichier("fichiers/scores.txt");

    std::string ligne;

    std::cout<<std::endl;

    if(fichier)
    {
        std::cout<<" "<<std::endl<<std::endl;
        std::cout << "                                             �     �     �" << std::endl;
        std::cout << "                                             ��   � �   ��" << std::endl;
        std::cout << "                                             � � �   � � �" << std::endl;
        std::cout << "                                             �  �     �  �" << std::endl;
        std::cout << "                                             �           �" << std::endl;
        std::cout << "                                             �������������" << std::endl << std::endl;
        std::cout << "                                             !SCORES!" << std::endl << std::endl << std::endl;
        while(getline(fichier, ligne))
        {

            std::cout /*<< "      No." << rang << " " */<< ligne << " Points!!!" << std::endl << std::endl;
            rang++;
        }
        std::cout << std::endl << std::endl;

    }
    else
    {
        std::cout << "Desole, nous n'avons pas trouve le fichier de sauvegarde" << std::endl;
        system("pause");

    }


}


Niveau chargerPartie(std::string userName, int& niv)
{
    std::string temp;

    std::string path="fichiers/";

    path+=userName;

    path+=".txt";

    std::ifstream fic(path.c_str());

    ActeurMobile snoopy('S',0,0);
    std::vector <ActeurMobile> balles;
    std::vector <Acteur> blocsC;
    std::vector <ActeurMobile> blocsP;
    std::vector <Acteur> blocsT;
    std::vector <Acteur> oiseaux;
    int score=0;
    int niveau=0;
    int vies=0;
    int temps=0;

    int x=0, y=0;
    int dx=0, dy=0;
    bool decalage=false;



    if(fic)
    {

        while(std::getline(fic,temp))
        {
            //std::cout<<temp<<std::endl;

            if(temp[3]!=' ')
            {
                x=10*conv(temp[2])+conv(temp[3]);
                y=conv(temp[5]);

                decalage=true;
            }
            else
            {
                x=conv(temp[2]);
                y=conv(temp[4]);

                decalage=false;
            }
            ///On test le 1er char de la ligne -> type acteur
            if(temp[0]=='S')
            {
                //les 3eme et 5eme char sont les coord (� convertir en int)
                snoopy.setCoord(x,y);
            }

            if(temp[0]=='B')
            {

                if(!decalage)
                {
                    if(temp[6]!='-')
                    {
                        dx= conv(temp[6]);
                        if(temp[8]!='-')
                            dy=conv(temp[8]);
                        else
                            dy=-conv(temp[9]);
                    }
                    else
                    {
                        dx=-conv(temp[7]);
                        if(temp[9]!='-')
                            dy=conv(temp[9]);
                        else
                            dy=-conv(temp[10]);
                    }
                }
                else
                {
                    if(temp[7]!='-')
                    {
                        dx= conv(temp[7]);
                        if(temp[9]!='-')
                            dy=conv(temp[9]);
                        else
                            dy=-conv(temp[10]);
                    }
                    else
                    {
                        dx=-conv(temp[8]);
                        if(temp[10]!='-')
                            dy=conv(temp[10]);
                        else
                            dy=-conv(temp[11]);
                    }
                }
                ActeurMobile balle('B',x,y,dx,dy);
                balles.push_back(balle);
            }
            if(temp[0]=='C')
            {
                Acteur blocC('C',x,y);
                blocsC.push_back(blocC);
            }
            if(temp[0]=='P')
            {
                ActeurMobile blocP('P',x,y);
                blocsP.push_back(blocP);
            }
            if(temp[0]=='T')
            {
                Acteur blocT('T',x,y);
                blocsT.push_back(blocT);
            }
            if(temp[0]=='O')
            {
                Acteur oiseau('O',x,y);
                oiseaux.push_back(oiseau);
            }

            if(temp[0]=='s')
            {
                for(unsigned int i=0; i<temp.size(); i++)
                {
                    score+=(pow(10,temp.size()-i-1))*(conv(temp[i]));
                }
            }
            if(temp[0]=='n')
            {
                niveau=conv(temp[2]);
                niv=niveau;
            }
            if(temp[0]=='v')
            {
                vies=conv(temp[2]);
            }
            if(temp[0]=='t')
            {
                for(unsigned int i=0; i<temp.size(); i++)
                {
                    temps+=(pow(10,temp.size()-i-1))*(conv(temp[i]));
                }
            }
        }
        ///On cree un niveau possedant toutes les caracteristiques
        std::vector < std::vector<char> > tab;
        for(int i=0; i<X_MAX; i++)
        {
            tab.push_back(std::vector<char> (Y_MAX, ' '));
        }

        //on declare un niveau grace au constructeur surcharg�
        Niveau level(niveau, score,userName, vies, temps, snoopy, balles, blocsC, blocsP, blocsT, oiseaux, tab);
        std::cout<<"Partie chargee avec succes, Niveau : "<<level.getDifficulte()<<" vies :"<<level.getnbvies()<<std::endl;

        return level;
    }
    else
    {
        std::cout<<"Impossible d'ouvrir le fichier!";
        niv=0;
        Niveau nul;
        nul.setDifficult(0);
        return nul;
    }
    /* //VERIFICATION DE L4EXACTITUDE DU RESULTAT

        std::cout<<"Snoopy x : "<<snoopy.getX()<<" / y : "<<snoopy.getY()<<std::endl;
        for(int i=0; i<balles.size(); i++)
        {
            std::cout<<"Balle x : "<<balles[i].getX()<<" / y : "<<balles[i].getY()<<" / dx : "<<balles[i].getDepX()<<" / dy : "<<balles[i].getDepY()<<std::endl;
        }
        for(int i=0; i<oiseaux.size(); i++)
        {
            std::cout<<"Oiseau x : "<<oiseaux[i].getX()<<" / y : "<<oiseaux[i].getY()<<std::endl;
        }

        for(int i=0; i<blocsC.size(); i++)
        {
            std::cout<<"BlocsC x : "<<blocsC[i].getX()<<" / y : "<<blocsC[i].getY()<<std::endl;
        }


        std::cout<<"Score : "<<score<<std::endl;
        std::cout<<"Niveau : "<<niveau<<std::endl;
        std::cout<<"Vies : "<<vies<<std::endl;
        std::cout<<"Temps : "<<temps<<std::endl;

        std::vector < std::vector <char> > tab;
        for(int i=0; i<X_MAX; i ++)
        {
            tab.push_back(std::vector<char> (Y_MAX,' '));
        }
    */

}
//convertit un char en chiffre
int conv(char caract)
{
    if(caract=='0')
        return 0;
    if(caract=='1')
        return 1;
    if(caract=='2')
        return 2;
    if(caract=='3')
        return 3;
    if(caract=='4')
        return 4;
    if(caract=='5')
        return 5;
    if(caract=='6')
        return 6;
    if(caract=='7')
        return 7;
    if(caract=='8')
        return 8;
    if(caract=='9')
        return 9;
    return 0;
}

void affichageMenu(Console* pConsole)
{
    pConsole->gotoLigCol(0,0);

    system("color 70");
    std::cout << std::endl;
    std::cout << " �     ��     ���   ���� �   �  ��  �   �  ��  �  � ����         ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������������";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "                      " << std::endl;
    std::cout << " �    �  �    �  �  �    �   � �  � ��  � �  � �  � �          ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(WHITE,WHITE);
    std::cout << "            ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������������";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "          " << std::endl;
    std::cout << " �    ����    ���   ���  �   � ���� � � � �    ���� ���      ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK, WHITE);
    std::cout << "                          ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "      " << std::endl;
    std::cout << " �    �  �    �  �  �     � �  �  � �  �� �  � �  � �      ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "    " << std::endl;
    std::cout << " ���� �  �    �  �  ����   �   �  � �   �  ��  �  � ����   ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����" << std::endl;
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "                                                           ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����" << std::endl;
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << " ���  ����    ��� �   �  ��   ��  ���  �   �             ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                                    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "  " << std::endl;
    std::cout << " �  � �      �    ��  � �  � �  � �  �  � �              ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                              ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "  " << std::endl;
    std::cout << " �  � ���     ��  � � � �  � �  � ���    �               ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��������";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                        ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "    " << std::endl;
    std::cout << " �  � �         � �  �� �  � �  � �     �                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "      " << std::endl;
    std::cout << " ���  ����   ���  �   �  ��   ��  �    �                 ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "      ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "          ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "        " << std::endl;
    std::cout << "                                                         ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��������";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "        ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����������";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "            " << std::endl;
    std::cout << "                                                           ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "                  " << std::endl;
    std::cout << "                                                           ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "        ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "        ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "    " << std::endl;
    std::cout << "                                                             ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��������";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "  ";
    pConsole->_setColor(RED,RED);
    std::cout << "RRRR";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "  " << std::endl;
    std::cout << " 1. Nouveau Jeu                  Utilisez les touches                    ";
    pConsole->_setColor(RED,RED);
    std::cout << "RRRR";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "        ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "    " << std::endl;
    std::cout << "                                                                         ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "              ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "    " << std::endl;
    std::cout << " 2. Charger Partie               'i' et 'k' pour naviguer                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "          ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "          " << std::endl;
    std::cout << "                                                                         ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "      ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "              " << std::endl;
    std::cout << " 3. Entrer Un Mot De Passe       en haut et en bas                     ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "            " << std::endl;
    std::cout << "                                                                       ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "            ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "            " << std::endl;
    std::cout << " 4. Comment Jouer                et appuyez sur la barre               ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "            ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "            " << std::endl;
    std::cout << "                                                                       ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "            ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "            " << std::endl;
    std::cout << " 5. Scores                       d'espace pour valider                 ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "            ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "            " << std::endl;
    std::cout << "                                                                     ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "        ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "              " << std::endl;
    std::cout << " 6. Quitter                      votre choix                     ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "                " << std::endl;
    std::cout << "                                                                   ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����������";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "      ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "              " << std::endl;
    std::cout << "                                                                       ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "        ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "          " << std::endl;
    std::cout << "                                                                       ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "        " << std::endl;
    std::cout << "                                                                     ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "        " << std::endl;
    std::cout << "                                                                       ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������������������";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "          " << std::endl;
    std::cout << " Copyright, TradeMark, All Rights Reserved, AG.JP.ES&Co." << std::endl << std::endl;

}

void regles(Console* pConsole)
{
    std::cout << std::endl << "                      COMMENT JOUER" << std::endl << std::endl;
    std::cout << "  Pour faire se deplacer Snoopy, utilisez les touches 'I', 'J', 'K' et 'L' pour Le faire bouger en haut," << std::endl << "  a gauche, en bas et a droite." << std::endl << std::endl;
    std::cout << "  Appuyez sur la touche 'A' pour casser un bloc de type 'C'" << std::endl << "  Marchez contre un bloc de type 'P' pour le pousser(vous ne pouvez que le pousser une fois)" << std::endl << "  Evitez les blocs de type 'T' et les balles 'B', faites nous confiance, on sait de quoi on parle" << std::endl << std::endl;
    std::cout << "  Rammasez les quatres Oiseaux 'O' au coins de chaque niveau dans le temps imparti pour gagner" << std::endl << std::endl;
    std::cout << "  Sauvegardez votre partie a tout moment en appuyant sur la touche 'S'" << std:: endl << std::endl;
    std::cout << "                                                                  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "        " << std::endl;
    std::cout << "                                                                  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(YELLOW,YELLOW);
    std::cout << "YY";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "        " << std::endl;
    std::cout << "               Bonne chance ;)                                    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(YELLOW,YELLOW);
    std::cout << "YYYY";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "    " << std::endl;
    std::cout << "                                                                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(YELLOW,YELLOW);
    std::cout << "YY";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "    " << std::endl;
    std::cout << "                                  -Woodstock              ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������";
    pConsole->_setColor(YELLOW,YELLOW);
    std::cout << "YYYYYYYYYY";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "    " << std::endl;
    std::cout << "                                                        ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(YELLOW,YELLOW);
    std::cout << "YYYYYYYYYYYYYYYYYY";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "  " << std::endl;
    std::cout << "                                                      ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(YELLOW,YELLOW);
    std::cout << "YYYYYY";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(YELLOW,YELLOW);
    std::cout << "YYYYYY";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "    " << std::endl;
    std::cout << "                                                      ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(YELLOW,YELLOW);
    std::cout << "YYYYYYYYYYYYYYYY";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "  " << std::endl;
    std::cout << "                                                        ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������";
    pConsole->_setColor(YELLOW,YELLOW);
    std::cout << "YYYYYYYY";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "      " << std::endl;
    std::cout << "                                                              ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(YELLOW,YELLOW);
    std::cout << "YY";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "      " << std::endl;
    std::cout << "                                                              ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(YELLOW,YELLOW);
    std::cout << "YY";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(YELLOW,YELLOW);
    std::cout << "YY";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "      " << std::endl;
    std::cout << "                                                              ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(YELLOW,YELLOW);
    std::cout << "YY";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(YELLOW,YELLOW);
    std::cout << "YY";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "  " << std::endl;
    std::cout << "                                                                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(YELLOW,YELLOW);
    std::cout << "YY";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(YELLOW,YELLOW);
    std::cout << "YYYY";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "  " << std::endl;
    std::cout << "                                                                  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(YELLOW,YELLOW);
    std::cout << "YYYYYY";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "  " << std::endl;
    std::cout << "                                                                    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(YELLOW,YELLOW);
    std::cout << "YY";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "  " << std::endl;
    std::cout << "                                                                      ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "  " << std::endl;
}
