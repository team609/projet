#include "partie.h"
#include <fstream>


///Constructeur "par defaut"
Partie::Partie(Niveau n1, Niveau n2, Niveau n3)
    : m_lvlcourant(), m_lvl1(n1), m_lvl2(n2), m_lvl3(n3), m_niveau(1), m_score(0), m_fin(false), m_pseudo("")
{

}

Partie::~Partie()
{

}


///SETTERS/GETTERS


void Partie::setNiveau(int niveau)
{
    if(niveau>0 && niveau<=15)
        m_niveau=niveau;
}

void Partie::setCharge(bool charge)
{
    m_lvlcourant.setCharge(charge);
}

void Partie::setLvl(Niveau lvl)
{
    if(lvl.getDifficulte()!=0)
        m_lvlcourant=lvl;
        m_lvlcourant.setCharge(true);
}

bool Partie::getCharge() const
{
    //on appelle le getter de Niveau
    return m_lvlcourant.getCharge();
}


int Partie::getNiveau() const
{
    return m_niveau;
}


Niveau Partie::getLvl() const
{
    return m_lvlcourant;
}

void Partie::setFin(bool fin)
{
    m_fin=fin;
}



void Partie::lancerPartie(Console* pConsole)
{
    boucle(pConsole);
}

void Partie::boucle(Console* pConsole)
{

    while(!m_fin)
    {

        if(!getCharge())
            m_lvlcourant=choixNiveau();

        /*std::cout<<"init...";
        pConsole->pause();*/


       /*m_lvl.boucle(pConsole);
        m_score+=m_lvl.getScore();*/




        m_lvlcourant.boucle(pConsole);

        m_score+=m_lvlcourant.getScore();
        m_lvlcourant.setCharge(false);

        //Quand le niveau est fini...
        pConsole->clearConsole();

        std::cout<<"Que voulez vous faire?"<<std::endl;
        int choix=0;

        //si le niveau n'est pass le dernier et que le niveau a �t� gagn�
       /* if((m_niveau<3)&&(m_lvl.getnboiseaux()==0))*/
        if((m_niveau<3)&&(m_lvlcourant.getnboiseaux()==0))
        {
            m_lvlcourant.finNiveau();
            m_lvlcourant.libererActeur();
            m_lvlcourant.effacerMatrice();

            std::cout<<"1->Recommencer niveau "<<m_niveau<<std::endl;
            std::cout<<"2->Passer au niveau "<<m_niveau +1 <<std::endl;
            std::cout<<"3->Sauvegarder et quitter "<<std::endl;

            //saisie blind�e entre 1 et 3
            choix=saisie_int(choix,1,3);

            switch(choix)
            {
            case 1 :
                pConsole->clearConsole();
                std::cout<<"On recommence !"<<std::endl;
                pConsole->pause();
                break;

            case 2 :
                pConsole->clearConsole();
                setNiveau(m_niveau+1);
                m_lvlcourant.setDifficult(m_niveau);
                std::cout<<"Niveau : "<<m_niveau;
                pConsole->pause();
                break;

            case 3:
                pConsole->clearConsole();
                sauvegardeScore();
                pConsole->pause();
                m_fin=true;
                break;
            }
        }
        //sinon on on recommence le niveau ou bien on quitte
        else
            {

                m_lvlcourant.finNiveau();
                m_lvlcourant.libererActeur();
                m_lvlcourant.effacerMatrice();



                std::cout<<"1->Recommencer niveau "<<m_niveau<<std::endl;
                std::cout<<"2->Enregistrer score et quitter "<<std::endl;
                std::cout<<"3->Quitter"<<std::endl;

                choix=saisie_int(choix,1,3);

                switch(choix)
                {
                case 1 :
                    pConsole->clearConsole();
                    std::cout<<"On recommence !"<<std::endl;
                    pConsole->pause();
                    break;

                case 2 :
                    pConsole->clearConsole();
                    sauvegardeScore();
                    pConsole->pause();
                    m_fin=true;
                    break;

                case 3 :
                    m_fin=true;
                    break;

                }

            }

            m_lvlcourant.finNiveau();
            m_lvlcourant.libererActeur();

    }
}


void Partie::sauvegardeScore()
{
    std::ofstream fichier("fichiers/scores.txt",std::ios::app);

    if(fichier)
    {
        //blindage pseudo non vide
        while(m_pseudo=="")
        {
            std::cout<<"Saisir le pseudo pour la savegarde"<<std::endl;
            std::getline(std::cin,m_pseudo);
        }

        //ecriture dans le fichier
        fichier<<m_pseudo<<" "<<m_score<<std::endl;
        std::cout<<"Sauvegarde effectuee avec succes !"<<std::endl;
    }
    else
    {
        //si le fichier ne s'ouvre pas
        std::cout<<"Ouverture du fichier impossible..."<<std::endl;
    }

    fichier.close();

}

Niveau Partie::choixNiveau()
{
    switch(m_niveau)
    {
    case 1:
        return m_lvl1;
        break;

    case 2:
        return m_lvl2;
        break;

    case 3:
        return m_lvl3;
        break;

    default :
        return m_lvl1;
        break;
    }
}

