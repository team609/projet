#ifndef CONSOLE_H_INCLUDED
#define CONSOLE_H_INCLUDED

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <string>
#include <fstream>

#define X_MAX 20
#define Y_MAX 10

//genere un nb aleatoire entre inf et sup
int alea(int inf, int sup);

//retourne un int saisi entre inf et sup
int saisie_int(int nombre, int inf, int sup);



enum Color
{
    BLACK = 0,
    WHITE = 15,
    RED = 12,
    GREEN = 2,
    BLUE = 9,
    YELLOW = 14,
    PURPLE = 5,
    DEFAULT = 7, // gris couleur de base de la console
};

class Console
{
    private:
        // Empecher la cr�ation
        Console();
        ~Console();

        // Empecher la copie d'objet...
        Console& operator= (const Console&){ return *this;}
        Console (const Console&){}

        // Attributs
        static Console* m_instance;

    public:

        void _setColor(int front, int back);
        // M�thodes statiques (publiques)
        static Console* getInstance();
        static void deleteInstance();

        // M�thodes publiques

        //positionne le curseur aux coord(lig,col)
        void gotoLigCol(int lig, int col);

        //retourne true si une touche est pressee
        bool isKeyboardPressed();

        //retourne la valeur de la touche du clavier pressee
        char getInputKey();

        //change la couleur
        void setColor(Color col);

        //efface la console
        void clearConsole();

        //fais une pause dans le programme tant qu'une touche n'est pas press�e
        void pause();

        //affiche ou non le curseur
        void ShowConsoleCursor(bool showFlag);
};



#endif // CONSOLE_H_INCLUDED
