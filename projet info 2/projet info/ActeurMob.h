#ifndef ACTEUR_MOBILE_H_INCLUDED
#define ACTEUR_MOBILE_H_INCLUDED

#include <iostream>
#include "Acteur.h"

class ActeurMobile : public Acteur
{
private:

    int m_dx, m_dy;
    int m_precdx, m_precdy;
    int m_mx, m_my;
    bool m_peutBouger;


public:
    ActeurMobile();
    ActeurMobile(char type, int x, int y, int dx=0, int dy=0);
    ~ActeurMobile();

    void setDepX(int dx);
    void setDepY(int dy);
    void setPrecDX();
    void setPrecDY();
    void setPeutBouger(bool peutBouger);

    int getDepX();
    int getDepY();
    int getPrecDX();
    int getPrecDY();
    bool getPeutBouger() const;

    //set les deplacements � 0
    void stop();

    //actualise les x et y
    void deplacement();

    //efface la case aux anciennes coordonnees
    void effaceTab(std::vector< std::vector <char> >& tab);

    //si l'acteur est en collision avec la cible
    bool collision(Acteur const& cible);

    //retourne un bool = true si le bloc n'est pas "bloqu�" par un acteur/le bord
    bool blocPeutBouger(int direction, std::vector< std::vector <char> >const& tab);
};


#endif // ACTEUR_MOBILE_H_INCLUDED
