#ifndef ACTEUR_H_INCLUDED
#define ACTEUR_H_INCLUDED

#include "console.h"


class Acteur
{

//modifi�/lu par acteur_mobile
protected:
    int m_x, m_y;
    int m_couleur;
    char m_type;
    bool m_alive;

public:
    //Constructeurs-Destructeurs
    Acteur();
    Acteur(Acteur const& copie);
    Acteur(char type, int x, int y);
    ~Acteur();

    //setters
    void setCoord(int x, int y);
    void setX(int x);
    void setY(int y);
    void setAlive(bool alive);
    void setType(char type);

    //Getters
    int getX() const;
    int getY() const;
    bool vivant() const;
    char type() const;

    //Methodes

    //envoie son type aux coordonn�es � la matrice
    void envoiTab(std::vector<std::vector<char> >& tab);

    //efface la position de l'acteur s'il est mort
    void effacer(std::vector< std::vector <char> >& tab);

    //verifie sur les cot�s s'il y a un acteur mobile
    bool verifGauche(Acteur const& act);
    bool verifDroite(Acteur const& act);
    bool verifHaut(Acteur const& act);
    bool verifBas(Acteur const& act);

};
#endif // ACTEUR_H_INCLUDED

