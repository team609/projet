#include "Acteur.h"

///Constructeur par defaut
Acteur::Acteur()
{
    m_alive=true;
    m_x=0;
    m_y=0;
    m_type='0';
    m_couleur=1;
}

///Constructeur par copie
Acteur::Acteur(Acteur const& copie)
{
    m_alive=copie.m_alive;
    m_x=copie.m_x;
    m_y=copie.m_y;
    m_type=copie.m_type;
}

///Constructeur surchargé
Acteur::Acteur(char type, int x, int y)
{
    m_alive=true;
    m_x=x;
    m_y=y;
    m_type=type;
    m_couleur=1;
}


Acteur::~Acteur()
{
    //pas de pointeur ici
}

void Acteur::setCoord(int x, int y)
{
    setX(x);
    setY(y);
}

void Acteur::setX(int x)
{
    if(x>=0 && x<X_MAX)
        m_x=x;
}

void Acteur::setY(int y)
{
    if(y>=0 && y<Y_MAX)
        m_y=y;
}

void Acteur::setAlive(bool alive)
{
    m_alive=alive;
}

void Acteur::setType(char type)
{
    m_type=type;
}

int Acteur::getX() const
{
    return m_x;
}

int Acteur::getY() const
{
    return m_y;
}

bool Acteur::vivant() const
{
    return m_alive;
}

char Acteur::type() const
{
    return m_type;
}

void Acteur::envoiTab(std::vector< std::vector<char> >& tab)
{
    if(vivant())
    {
        tab[getX()][getY()]=type();
    }
}

void Acteur::effacer(std::vector< std::vector<char> >& tab)
{
    if(!vivant())
    {
        tab[getX()][getY()]=' ';
    }
}



bool Acteur::verifGauche(Acteur const& act)
{
    if( (act.getX()==getX()+1) && (act.getY()==getY()) )
    {
        return true;
    }
    else
    {
        return false;
    }

}

bool Acteur::verifDroite(Acteur const& act)
{
    if( (act.getX()==getX()-1) && (act.getY()==getY()) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Acteur::verifHaut(Acteur const& act)
{
    if( (act.getX()==getX()) && (act.getY()==getY()+1) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Acteur::verifBas(Acteur const& act)
{
    if( (act.getX()==getX()) && (act.getY()==getY()-1) )
    {
        return true;
    }
    else
    {
        return false;
    }
}
