#ifndef NIVEAU_H_INCLUDED
#define NIVEAU_H_INCLUDED

#ifndef MATRICE_H_INCLUDED
#define MATRICE_H_INCLUDED

#include "console.h"
#include "ActeurMob.h"
#include "Acteur.h"


void setCoordonnees(int& x, int& y);
void affichageCouleurs(char act, Console* pConsole);
void setDep(int& dx, int& dy);

class Niveau
{

private:

    ///ATTRIBUTS


    int m_score;
    int m_difficulte;
    int m_vies;
    bool m_fin;
    clock_t m_debut;
    clock_t m_temps;
    std::string m_pseudo;
    bool m_charge;
    clock_t m_tempsEcoule;

    //Matrice
    std::vector< std::vector <char> > m_tab;

    //Snoopy
    ActeurMobile m_snoopy;

    //Tous les tableaux d'acteurs :
    std::vector <Acteur> m_blocsT; //blocs pieges
    std::vector <ActeurMobile> m_blocsP; //blocs poussables
    std::vector <Acteur> m_blocsC;  //blocs cassables
    std::vector <Acteur> m_oiseaux;
    std::vector <ActeurMobile> m_balles;

    int m_nbboucles;

public :

    Niveau();
    Niveau(Niveau const& copie);
    Niveau(int niv, int score, std::string pseudo, int vies,int temps, ActeurMobile snoopy, std::vector<ActeurMobile> balles, std::vector<Acteur> blocsC, std::vector<ActeurMobile> blocsP,std::vector<Acteur> blocsT,std::vector<Acteur> oiseaux,std::vector <std::vector <char> > tab);


    ///Getters/setters
    void setDifficult(int diff);
    void setCharge(bool charge);
    bool getCharge() const;
    int getDifficulte() const;
    int getnboiseaux() const;
    int getnbvies() const;
    int getScore() const;
    bool getFin() const;

    //initialise aleatoiremennt les blocs (tailles recues en parametre)
    void initialiser(int nbC, int nbP, int nbT, int nbB, int nivea);

    //boucle de jeu (tant que le jeu n'est pas fini -> appel des sous programmes)
    void boucle(Console* pConsole);

    //actualise les tableaux d'acteur et les valeurs (temps...)
    void actualiserTab(Console* pconsole);

    //affichage matrice/infos sur le jeu
    void afficher(Console* pConsole);

    //gestion clavier (deplacement...)
    void clavier(Console* pConsole);

    //gestion si snoopy mange un oiseau ou bien casse un bloc
    void gestionCollisions(Console* pConsole, bool appuiKeyA);

    //la (les) balles rebondissent sur les blocs
    void collisionBalles();

    //si il n'y a plus d'oiseau...
    void gagner(Console* pConsole);

    //si on meure on perd une vie
    void perdreVie(Console* pConsole);

    //quand on a plus de vie
    void perdre(Console* pConsole);

    //si on rentre dans un bloc T ou une balle
    bool collisionMortelle();

    //sauvegarde les acteurs dans un fichier txt
    void sauvegarder(Console* pConsole);

    //regarde si le pseudo est deja pris
    bool verifPseudoUnique(Console* pConsole);

    //pause...
    void pause(Console* pConsole);

    //si snoopy est bloqu� par un des blocs
    bool bloquerGauche();
    bool bloquerDroite();
    bool bloquerHaut();
    bool bloquerBas();

    //si un acteur dois mourir, il est retir� du tableau
    void libererActeur();

    //a la fin du niveau, tous les acteurs meurent
    void finNiveau();

    //efface toute la matrice
    void effacerMatrice();
};

#endif // MATRICE_H_INCLUDED


#endif // NIVEAU_H_INCLUDED
