#include "Niveau.h"
#include "Jeu.h"
#include "Acteur.h"


///Constructeur par defaut
Niveau::Niveau()
{
    int i=0;

    ///INIT TAB
    for(i=0; i<X_MAX; i++)
    {
        m_tab.push_back(std::vector<char> (Y_MAX, ' '));
    }

    m_charge=false;
    m_difficulte=1;
    m_vies=3;
    m_fin=false;
    m_pseudo="";
    m_temps=time(NULL);
    m_score=0;
    m_nbboucles=0;
}


///Constructeur par copie
Niveau::Niveau(Niveau const& copie)
{
    m_difficulte=copie.m_difficulte;
    m_score=copie.m_score;
    m_pseudo=copie.m_pseudo;
    m_vies=copie.m_vies;
    m_temps=copie.m_temps;
    m_tempsEcoule=copie.m_tempsEcoule;
    m_fin=copie.m_fin;

    m_tab=copie.m_tab;
    m_snoopy.setX(copie.m_snoopy.getX());
    m_snoopy.setY(copie.m_snoopy.getY());
    m_snoopy.setType('S');
    m_balles=copie.m_balles;
    m_blocsC=copie.m_blocsC;
    m_blocsP=copie.m_blocsP;
    m_blocsT=copie.m_blocsT;
    m_oiseaux=copie.m_oiseaux;

}

///Constructeur surcharg�
Niveau::Niveau(int niv, int score, std::string pseudo, int vies,int temps, ActeurMobile snoopy, std::vector<ActeurMobile> balles, std::vector<Acteur> blocsC, std::vector<ActeurMobile> blocsP,std::vector<Acteur> blocsT,std::vector<Acteur> oiseaux,std::vector <std::vector <char> > tab)
{
    m_difficulte=niv;
    m_score=score;
    m_pseudo=pseudo;
    m_vies=vies;
    m_debut=clock();
    m_tempsEcoule=temps;
    m_fin=false;
    m_charge=false;

    m_tab=tab;

    m_snoopy.setX(snoopy.getX());
    m_snoopy.setY(snoopy.getY());
    m_snoopy.setType('S');

    m_balles=balles;
    m_blocsC=blocsC;
    m_blocsP=blocsP;
    m_blocsT=blocsT;
    m_oiseaux=oiseaux;

}

///Initialisation aleatoire en fonctin des parametres
void Niveau::initialiser(int nbC, int nbP, int nbT, int nbB, int niveau)
{
    int x=0, y=0;

    m_fin=false;
    m_vies=3;
    m_score=0;

    if(m_difficulte>0)
    {
        m_snoopy.setType('S');
        m_snoopy.setCoord(X_MAX/2,Y_MAX/2);
        m_snoopy.setDepX(0);
        m_snoopy.setDepY(0);

        for(int i=0; i<nbC; i++)
        {
            setCoordonnees(x,y);
            Acteur blocTemp('C',x,y);
            m_blocsC.push_back(blocTemp);
        }

        for(int i=0; i<nbP; i++)
        {
            setCoordonnees(x,y);
            ActeurMobile blocTemp('P',x,y);
            m_blocsP.push_back(blocTemp);
        }
        for(int i=0; i<nbT; i++)
        {
            setCoordonnees(x,y);
            Acteur blocTemp('T',x,y);
            m_blocsT.push_back(blocTemp);
        }
        for(int i=0; i<nbB; i++)
        {
            setCoordonnees(x,y);
            int dx=0, dy=0;
            setDep(dx,dy);
            ActeurMobile balle('B',x,y,dx,dy);
            m_balles.push_back(balle);
        }

        ///INIT OISEAUX
        Acteur oiseauTemp('O',0,0);
        m_oiseaux.push_back(oiseauTemp);
        oiseauTemp.setCoord(0,Y_MAX-1);
        m_oiseaux.push_back(oiseauTemp);
        oiseauTemp.setCoord(X_MAX-1,0);
        m_oiseaux.push_back(oiseauTemp);
        oiseauTemp.setCoord(X_MAX-1,Y_MAX-1);
        m_oiseaux.push_back(oiseauTemp);
    }
}



///GETTERS SETTERS

bool Niveau::getFin() const
{
    return m_fin;
}

int Niveau::getDifficulte() const
{
    return m_difficulte;
}

int Niveau::getnboiseaux() const
{
    return m_oiseaux.size();
}

int Niveau::getnbvies() const
{
    return m_vies;
}

int Niveau::getScore() const
{
    return m_score;
}

void Niveau::setDifficult(int diff)
{
    if(diff>0 && diff<=15)
        m_difficulte=diff;
}

void Niveau::setCharge(bool charge)
{
    m_charge=charge;
}

bool Niveau::getCharge() const
{
    return m_charge;
}



///Retourne un booleen = true si Snoopy est bloqu� par un bloc (cassable ou poussable)

//on regarde a gauche
bool Niveau::bloquerGauche()
{
    unsigned int i=0;

    for(i=0; i<m_blocsC.size(); i++)
    {
        if(m_blocsC[i].verifGauche(m_snoopy))
            return true;
    }

    ///Pour les blocs P on check aussi pour son deplacement
    for(i=0; i<m_blocsP.size(); i++)
    {
        //Verifie si le bloc "bloc" Snoopy
        if(m_blocsP[i].verifGauche(m_snoopy))
        {
            if(m_blocsP[i].getPeutBouger()) //Verifie si le bloc a deja boug�
            {
                if(m_blocsP[i].blocPeutBouger(1, m_tab)) //Verifie si le bloc est coinc� (position 1 : gauche)
                {
                    m_blocsP[i].setPeutBouger(false); //Changer le booleen
                    m_blocsP[i].setDepX(-1); //Initier le d�placement
                    return false;
                }
                return true;
            }
            return true;
        }
    }

    return false;
}

bool Niveau::bloquerDroite()
{
    unsigned int i=0;

    for(i=0; i<m_blocsC.size(); i++)
    {
        if(m_blocsC[i].verifDroite(m_snoopy))
            return true;
    }
    for(i=0; i<m_blocsP.size(); i++)
    {
        if(m_blocsP[i].verifDroite(m_snoopy))
        {
            if(m_blocsP[i].getPeutBouger())//Verifie si le bloc a deja boug�
            {
                if(m_blocsP[i].blocPeutBouger(2, m_tab)) //Verifie si le bloc est coinc� (position 2 : droite)
                {
                    m_blocsP[i].setPeutBouger(false); //Changer le booleen
                    m_blocsP[i].setDepX(1); //Initier le d�placement
                    return false;
                }
                return true;
            }
            return true;
        }
    }

    return false;
}


bool Niveau::bloquerHaut()
{
    unsigned int i=0;

    for(i=0; i<m_blocsC.size(); i++)
    {
        if(m_blocsC[i].verifHaut(m_snoopy))
            return true;
    }
    for(i=0; i<m_blocsP.size(); i++)
    {
        if(m_blocsP[i].verifHaut(m_snoopy))
        {
            if(m_blocsP[i].getPeutBouger()) //Verifie si le bloc a deja boug�
            {
                if(m_blocsP[i].blocPeutBouger(3, m_tab)) //Verifie si le bloc est coinc� 3 : haut
                {
                    m_blocsP[i].setPeutBouger(false); //Changer le booleen
                    m_blocsP[i].setDepY(-1); //Initier le d�placement
                    return false;
                }
                return true;
            }
            return true;
        }
    }

    return false;
}


bool Niveau::bloquerBas()
{
    unsigned int i=0;

    for(i=0; i<m_blocsC.size(); i++)
    {
        if(m_blocsC[i].verifBas(m_snoopy))
            return true;

    }

    for(i=0; i<m_blocsP.size(); i++)
    {
        if(m_blocsP[i].verifBas(m_snoopy))
        {
            if(m_blocsP[i].getPeutBouger()) //Verifie si le bloc a deja boug�
            {
                if(m_blocsP[i].blocPeutBouger(4, m_tab)) //Verifie si le bloc est coinc� 4 : bas
                {
                    m_blocsP[i].setPeutBouger(false); //Changer le booleen
                    m_blocsP[i].setDepY(1); //Initier le d�placement
                    return false;
                }
                return true;
            }
            return true;
        }
    }

    return false;
}

///Appel�e par la fonction de gestion clavier (recois un parametre vrai si on appuie sur 'a' pour casser un bloc C
void Niveau::gestionCollisions(Console* pConsole, bool appuiKeyA)
{
    unsigned int i=0;

    ///Snoopy "mange" un oiseau
    for(i=0; i<m_oiseaux.size(); i++)
    {
        if(m_snoopy.collision(m_oiseaux[i]))
        {
            m_oiseaux[i].setAlive(false);
            m_score+=1000; //on incremente le score :)
        }
    }

    ///Snoopy casse un bloc
    for(i=0; i<m_blocsC.size(); i++)
    {
        if(m_blocsC[i].verifGauche(m_snoopy) || m_blocsC[i].verifDroite(m_snoopy) || m_blocsC[i].verifHaut(m_snoopy) || m_blocsC[i].verifBas(m_snoopy))

        {
            if(appuiKeyA)
            {
                m_blocsC[i].setAlive(false);
                m_score+=25; //on incremente le score aussi :)
            }
        }
    }
}



void Niveau::boucle(Console* pConsole)
{
    pConsole->clearConsole();
    system("color F");
    m_debut=clock();

    while(!m_fin)
    {
        clavier(pConsole);
        collisionBalles();
        actualiserTab(pConsole);
        libererActeur();
        afficher(pConsole);
        gagner(pConsole);
        perdreVie(pConsole);
        perdre(pConsole);
    }

}

/*void Niveau::setBlocsC(std::vector <Acteur> blocs)
{
    m_blocsC=blocs;
}

void Niveau::setBlocsP(std::vector <Acteur> blocs)
{
    m_blocsP=blocs;
}*/

/*void Niveau::setOiseaux(std::vector <Acteur> oiseaux)
{
    m_oiseaux=oiseaux;
}*/



void Niveau::afficher(Console* pConsole)
{
    int i=0, j=0;

    pConsole->ShowConsoleCursor(false);

    ///Affichage des infos


    pConsole->gotoLigCol(75,10);
    std::cout<<m_blocsP[0].getPeutBouger();


    pConsole->gotoLigCol(70,0);
    std::cout<<"Niveau : "<<m_difficulte;
    pConsole->gotoLigCol(70,2);
    std::cout<<"Temps restant : "<<60-m_temps;
    pConsole->gotoLigCol(70,4);
    std::cout<<"Vies restantes : "<<m_vies;
    pConsole->gotoLigCol(70,6);
    std::cout<<"score : "<<m_score;

    //affichage de la bordure haute
    pConsole->gotoLigCol(5,4);
    for(i=0; i<X_MAX+11; i++)
        std::cout<<"**";

    for(i=0; i<Y_MAX; i++)
    {
        pConsole->gotoLigCol(5,5+i);
        std::cout<<"*"; //bordure cote gauche

        for(j=0; j< X_MAX; j++)
        {
            //en fonction du type on change la couleur
            switch(m_tab[j][i])
            {
            case 'S' :
                pConsole->_setColor(WHITE,BLACK);
                break;

            case 'B' :
                pConsole->_setColor(RED,BLACK);
                break;

            case 'C' :
                pConsole->_setColor(BLUE,BLACK);
                break;

            case 'P' :
                pConsole->_setColor(GREEN,BLACK);
                break;

            case 'T' :
                pConsole->_setColor(PURPLE,BLACK);
                break;

            case 'O' :
                pConsole->_setColor(YELLOW,BLACK);
                break;

            default:
                break;
            }
            //on affiche la case
            std::cout<<m_tab[j][i];
            pConsole->_setColor(WHITE,BLACK);
            std::cout << "  ";
        }

        pConsole->_setColor(DEFAULT,BLACK);
        std::cout<<"*"; //bordure cote droite

        std::cout<<std::endl<<std::endl;
    }

    //bordure dessous
    pConsole->gotoLigCol(5,4+Y_MAX+1);
    for(i=0; i<X_MAX+11; i++)
        std::cout<<"**";
}

void setCoordonnees(int& x, int& y)
{
    do
    {
        //x et y tir�s au hasard sans d�passer
        x=alea(0,X_MAX-1);
        y=alea(0,Y_MAX-1);
    }
    while((x==0&&y==0) || (x==0&&y==Y_MAX-1) || (x==X_MAX-1&&y==0) || (x==X_MAX-1&&y==Y_MAX-1) || (x==(X_MAX)/2&&y==(Y_MAX)/2));
    //coordonn�es differentes de celles des oiseaux/snoopy au debut
}

void setDep(int& dx, int& dy)
{
    dx=alea(1,1);
    int test=alea(0,1);
    if(test==0)
        dy=dx;
    if(test==1)
        dy=-dx;
}



void Niveau::clavier(Console* pConsole)
{
    bool appuiKeyA=false;


    if(pConsole->isKeyboardPressed())
    {
        char key=pConsole->getInputKey();

        switch(key)
        {
        case 'j' :
            //case 75:
            if(!bloquerGauche())
            {
                m_snoopy.setDepX(-1);
                m_snoopy.setDepY(0);
            }
            break;

        case 'l' :
            //case 77:
            if(!bloquerDroite())
            {
                m_snoopy.setDepX(1);
                m_snoopy.setDepY(0);
            }
            break;

        case 'i' :
            //case 72:
            if(!bloquerHaut())
            {
                m_snoopy.setDepY(-1);
                m_snoopy.setDepX(0);
            }
            break;

        case 'k' :
            //case 80:
            if(!bloquerBas())
            {
                m_snoopy.setDepY(1);
                m_snoopy.setDepX(0);
            }
            break;

        case 'a' :
            appuiKeyA=true;
            break;

        case 's' :
            sauvegarder(pConsole);
            break;

        case 'p' :
            pause(pConsole);
            break;
        case 'q' :
            m_fin=true;
            break;

        default:
            break;
        }
    }
    gestionCollisions(pConsole,appuiKeyA);
}

void Niveau::gagner(Console* pConsole)
{
    if(m_oiseaux.size()==0)
    {
        m_fin=true;
        pConsole->clearConsole();
        m_score+=(60-m_temps)*100;

        std::cout << "Bravo, vous avez gagne le niveau " << m_difficulte << " avec un score de : "<< m_score << " !" << std::endl << std::endl;

        pConsole->pause();
    }
}

void Niveau::actualiserTab(Console* pconsole)
{
    unsigned int i=0;

    //si le niveau n'a pa ete charge -> classique
    if(!getCharge())
        m_temps=(double)(clock()-m_debut)/CLOCKS_PER_SEC;
    else
    {
        //sinon on ajoute au temps le temps ecoul� dans la partie sauvergard�e
        m_temps=m_tempsEcoule+(clock()-m_debut)/CLOCKS_PER_SEC;
    }

    m_snoopy.deplacement();
    clavier(pconsole);

    pconsole->gotoLigCol(45,10);
    for(i=0; i<m_balles.size(); i++)
    {
        m_balles[i].effaceTab(m_tab);
    }

    for(i=0; i<m_oiseaux.size(); i++)
    {
        m_oiseaux[i].envoiTab(m_tab);
        m_oiseaux[i].effacer(m_tab);
    }

    for(i=0; i<m_blocsC.size(); i++)
    {
        m_blocsC[i].envoiTab(m_tab);
        m_blocsC[i].effacer(m_tab);
    }
    for(i=0; i<m_blocsP.size(); i++)
    {
        m_blocsP[i].deplacement();
        m_blocsP[i].envoiTab(m_tab);
        m_blocsP[i].effacer(m_tab);

        m_blocsP[i].stop();
    }
    for(i=0; i<m_blocsT.size(); i++)
    {
        m_blocsT[i].envoiTab(m_tab);
        m_blocsT[i].effacer(m_tab);
    }

    m_snoopy.effaceTab(m_tab);
    m_snoopy.envoiTab(m_tab);
    m_snoopy.stop();

    for(i=0; i<m_balles.size(); i++)
    {
        if(m_nbboucles%(20-5*m_difficulte)==0)
        {
            m_balles[i].deplacement();
        }

        m_balles[i].envoiTab(m_tab);
    }

    m_nbboucles++;

}

bool Niveau::collisionMortelle()
{
    unsigned int i=0;

    for(i=0; i< m_balles.size(); i++)
    {
        if(m_snoopy.collision(m_balles[i]))
            return true;
    }
    for(i=0; i<m_blocsT.size(); i++)
    {
        if(m_snoopy.collision(m_blocsT[i]))
            return true;
    }

    return false;

}

void Niveau::perdreVie(Console* pConsole)
{
    if(collisionMortelle())
    {
        pConsole->clearConsole();
        //pConsole->gotoLigCol(15,15);
        //std::cout<<"You loose! ";
        m_vies--;
        if(m_snoopy.getX()!=X_MAX/2 && m_snoopy.getY()!=Y_MAX/2)
            m_snoopy.setCoord(X_MAX/2,Y_MAX/2);
        else
            m_snoopy.setCoord(X_MAX/2 +1, Y_MAX/2);
    }
    else if(60-m_temps<=0)
    {
        //pConsole->clearConsole();
        //pConsole->gotoLigCol(45,15);
        std::cout<<"Temps ecoute !";
        m_vies--;
    }
}

void Niveau::perdre(Console* pConsole)
{
    //si le nb de vie est nulle
    if(m_vies<=0)
    {
        m_fin=true;

        pConsole->clearConsole();

        std::cout<<"Vous avez echoue..."<<std::endl;
        pConsole->pause();
    }


}

void Niveau::libererActeur()
{
    unsigned int i=0;

    ///on mets le booleen vivant de tous les acteurs � faux, la fonction libererMemoire se chargera de les effacer

    //OISEAUX
    for(i=0; i<m_oiseaux.size(); i++)
    {
        if(!m_oiseaux[i].vivant())
            m_oiseaux.erase(m_oiseaux.begin()+i);
    }

    //BLOCS C
    for(i=0; i<m_blocsC.size(); i++)
    {
        if(!m_blocsC[i].vivant())
            m_blocsC.erase(m_blocsC.begin()+i);
    }

    //BLOCS P
    for(i=0; i<m_blocsP.size(); i++)
    {
        if(!m_blocsP[i].vivant())
            m_blocsP.erase(m_blocsP.begin()+i);
    }

    //BLOCS T
    for(i=0; i<m_blocsT.size(); i++)
    {
        if(!m_blocsC[i].vivant())
            m_blocsT.erase(m_blocsT.begin()+i);
    }

    //BALLES
    for(i=0; i<m_balles.size(); i++)
    {
        if(!m_balles[i].vivant())
            m_balles.erase(m_balles.begin()+i);
    }


}

void Niveau::sauvegarder(Console* pConsole)
{
    unsigned int i=0;

    //on declare le chemin
    std::string fileName("fichiers/");

    //on demande quoi faire
    pConsole->clearConsole();
    pConsole->gotoLigCol(20,5);
    std::cout << "Saisir 1 pour sauvegarder la partie en cours"<< std::endl;
    pConsole->gotoLigCol(20,6);
    std::cout << "Saisir 2 pour annuler"<< std::endl;

    int annuler=0;

    annuler=saisie_int(annuler,1,2);

    //si il veut sauvegarder
    if(annuler!=2)
    {
        std::cout << "Entrez un pseudo " << std::endl;

        while(!verifPseudoUnique(pConsole)) {}
        fileName+=m_pseudo;
        fileName+=".txt";
        std::ifstream file(fileName.c_str());
        std::ofstream fichier(fileName.c_str(), std::ios::out | std::ios::trunc);
        if(fichier)
        {
            fichier << 'S' << ' ' << m_snoopy.getX() << ' ' << m_snoopy.getY() << std::endl;

            for (i=0; i<m_balles.size(); i++)
            {
                fichier << 'B' << ' ' << m_balles[i].getX() << ' ' << m_balles[i].getY() << ' ' << m_balles[i].getDepX() << ' ' << m_balles[i].getDepY() << std::endl;
            }

            for (i=0; i<m_oiseaux.size(); i++)
            {
                fichier << 'O' << ' ' << m_oiseaux[i].getX() << ' ' << m_oiseaux[i].getY() << std::endl;
            }

            for (i=0; i<m_blocsT.size(); i++)
            {
                fichier << 'T' << ' ' << m_blocsT[i].getX() << ' ' << m_blocsT[i].getY() << std::endl;
            }

            for (i=0; i<m_blocsP.size(); i++)
            {
                fichier << 'P' << ' ' << m_blocsP[i].getX() << ' ' << m_blocsP[i].getY()/* << ' ' << m_blocsP[i].getbooldep()*/ << std::endl;
            }

            for (i=0; i<m_blocsC.size(); i++)
            {
                fichier << 'C' << ' ' << m_blocsC[i].getX() << ' ' << m_blocsC[i].getY() << std::endl;
            }

            fichier << "n " << m_difficulte << std::endl;
            fichier << "t " << (60-m_temps) << std::endl;
            fichier << "v " << m_vies << std::endl;
            fichier << "s " << m_score << std::endl;

            fichier.close();

            pConsole->gotoLigCol(15,20);
            std::cout<<"Fichier enregistre avec succes !";
            pConsole->pause();
            m_fin=true;
        }
    }
    else
    {
        std::cout<<"Sauvegarde annulee, la partie va reprendre !"<<std::endl;
        pConsole->pause();
        pConsole->clearConsole();
    }

}

bool Niveau::verifPseudoUnique(Console* pConsole)
{
    bool pseudoUnique=false;

    while(!pseudoUnique)
    {
        //On prend le pseudo, on regarde s'il y a un fichier txt associ�

        std::getline(std::cin, m_pseudo);
        std::string fileName("fichiers/");
        fileName+=m_pseudo;
        fileName+=".txt";

        std::ifstream file(fileName.c_str());

        //si il existe, on le ferme, on demande de prendre un autre pseudo
        if(file)
        {
            file.close();
            std::cout << "Ce pseudo est deja pris, choisissez-en un nouveau" << std::endl;
        }
        else
        {
            //sinon c'est bon
            return true;
        }
    }
    return false;
}


void Niveau::collisionBalles()
{
    unsigned int i, c, p, t;
    bool dirXChange=false, dirYChange=false;

    for(i=0; i<m_balles.size(); i++)
    {
        for(c=0; c<m_blocsC.size(); c++)
        {
            if((m_balles[i].getX()+m_balles[i].getDepX()==m_blocsC[c].getX())&&((m_balles[i].getY()+m_balles[i].getDepY())==m_blocsC[c].getY()))
            {
                switch(rand()%2)
                {
                case 0:
                    m_balles[i].setDepY(((-1)*(m_balles[i].getDepY())));
                    dirYChange=true;
                    break;
                case 1:
                    m_balles[i].setDepX(-m_balles[i].getDepX());
                    dirXChange=true;
                    break;
                }
            }
        }

        for(p=0; p<m_blocsP.size(); p++)
        {
            if((m_balles[i].getX()+m_balles[i].getDepX()==m_blocsP[p].getX())&&((m_balles[i].getY()+m_balles[i].getDepY())==m_blocsP[p].getY()))
            {
                switch(rand()%2)
                {
                case 0:
                    m_balles[i].setDepY(((-1)*(m_balles[i].getDepY())));
                    if(dirYChange)
                    {
                        m_balles[i].setDepX(-m_balles[i].getDepX());
                    }
                    else
                    {
                        dirYChange=true;
                    }
                    break;
                case 1:
                    m_balles[i].setDepX(-m_balles[i].getDepX());
                    if(dirXChange)
                    {
                        m_balles[i].setDepY(-m_balles[i].getDepY());
                    }
                    else
                    {
                        dirXChange=true;
                    }
                    break;
                }
            }
        }

        for(t=0; t<m_blocsT.size(); t++)
        {
            if((m_balles[i].getX()+m_balles[i].getDepX()==m_blocsT[t].getX())&&((m_balles[i].getY()+m_balles[i].getDepY())==m_blocsT[t].getY()))
            {
                switch(rand()%2)
                {
                case 0:
                    m_balles[i].setDepY(((-1)*(m_balles[i].getDepY())));
                    if(dirYChange)
                    {
                        m_balles[i].setDepX(-m_balles[i].getDepX());
                    }
                    else
                    {
                        dirYChange=true;
                    }
                    break;
                case 1:
                    m_balles[i].setDepX(-m_balles[i].getDepX());
                    if(dirXChange)
                    {
                        m_balles[i].setDepY(-m_balles[i].getDepY());
                    }
                    else
                    {
                        dirXChange=true;
                    }
                    break;
                }
            }
        }
    }
}


void Niveau::pause(Console* pConsole)
{
    bool pause=true;

    //on prend la duree de la pause
    int temps=clock();

    for(unsigned int i=0; i<m_balles.size(); i++)
    {
        m_balles[i].setPrecDX();
        m_balles[i].setPrecDY();
        m_balles[i].stop();
    }

    while(pause)
    {

        m_snoopy.stop();

        if(pConsole->getInputKey()=='p')
            pause=false;

    }


    for(unsigned int i=0; i<m_balles.size(); i++)
    {
        m_balles[i].setDepX(m_balles[i].getPrecDX());
        m_balles[i].setDepY(m_balles[i].getPrecDY());
    }

    m_debut+=(clock()-temps);
}

void Niveau::finNiveau()
{
    unsigned int i=0;

    for(i=0; i<m_balles.size(); i++)
    {
        m_balles[i].setAlive(false);
    }
    for(i=0; i<m_blocsC.size(); i++)
    {
        m_blocsC[i].setAlive(false);
    }
    for(i=0; i<m_blocsP.size(); i++)
    {
        m_blocsP[i].setAlive(false);
    }
    for(i=0; i<m_blocsT.size(); i++)
    {
        m_blocsT[i].setAlive(false);
    }
}

void Niveau::effacerMatrice()
{
    int i=0, j=0;

    for(j=0; j<Y_MAX; j++)
    {
        for(i=0; i<X_MAX; i++)
        {
            m_tab[i][j]=' ';
        }
    }
}
