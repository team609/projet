#include "main.h"

using namespace std;

int main()
{
    system("mode con LINES=50 COLS=90");

    //on a besoin du hasard
    srand(time(NULL));

    //declaration/initialisation pointeur type Console
    Console* pConsole = NULL;
    pConsole = Console::getInstance();


    //On lance le menu
    menu(pConsole);



    system("color F");
    pConsole->clearConsole();



    Console::deleteInstance();

    return 0;
}
