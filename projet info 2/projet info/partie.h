#ifndef PARTIE_H_INCLUDED
#define PARTIE_H_INCLUDED

#include "console.h"
#include "Niveau.h"

class Partie
{
private:

    Niveau m_lvlcourant;
    Niveau m_lvl1;
    Niveau m_lvl2;
    Niveau m_lvl3;
    int m_niveau;
    int m_score;
    bool m_fin;
    std::string m_pseudo;

public:
    Partie(Niveau n1, Niveau n2, Niveau n3);
    Partie(int niveau);
    ~Partie();

    void setCharge(bool charge);
    void setNiveau(int niveau);
    void setLvl(Niveau lvl);
    void setFin(bool fin);

    bool getCharge() const;
    int getNiveau() const;
    Niveau getLvl() const;

    Niveau choixNiveau();

/*    void setCharge(bool charge) { m_lvlcourant.setCharge(charge); }
    void setNiveau(int niveau);
    void setLvl(Niveau lvl) { if(lvl.getDifficulte()!=0) m_lvlcourant=lvl; }

    bool getCharge() const { return m_lvlcourant.getCharge(); }
    int getNiveau() const { return m_niveau; }
    Niveau getLvl() const { return m_lvlcourant; }
>>>>>>> 4af143d813601f1348ea5c46b9b3a02d48e2f9fe*/

    //lance la partie
    void lancerPartie(Console* pConsole);

    //boucle de "jeu" (enchaine les niveaux)
    void boucle(Console* pConsole);

    //enregistre le score total dans un fichier txt
    void sauvegardeScore();
};

#endif // PARTIE_H_INCLUDED
