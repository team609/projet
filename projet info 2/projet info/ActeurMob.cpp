#include "ActeurMob.h"

///Constructeur par defaut
ActeurMobile::ActeurMobile()
{
    m_x=0;
    m_y=0;
    m_mx=m_x;
    m_my=m_y;
    m_dx=0;
    m_dy=0;
    m_precdx=0;
    m_precdy=0;
    m_type='0';
    m_peutBouger=true;
}

///Constructeur surchargé
ActeurMobile::ActeurMobile(char type, int x, int y, int dx, int dy)
{
    m_x=x;
    m_y=y;
    m_mx=m_x;
    m_my=m_y;
    m_dx=dx;
    m_dy=dy;
    m_precdx=0;
    m_precdy=0;
    m_type=type;
    m_peutBouger=true;
}

///Destructeur
ActeurMobile::~ActeurMobile()
{

}

///GETTERS & SETTERS
int ActeurMobile::getDepX()
{
    return m_dx;
}

int ActeurMobile::getDepY()
{
    return m_dy;
}

int ActeurMobile::getPrecDX()
{
    return m_precdx;
}

int ActeurMobile::getPrecDY()
{
    return m_precdy;
}


void ActeurMobile::setDepX(int dx)
{
    m_dx=dx;
}

void ActeurMobile::setDepY(int dy)
{
    m_dy=dy;
}

void ActeurMobile::setPrecDX()
{
    m_precdx=m_dx;
}

void ActeurMobile::setPrecDY()
{
    m_precdy=m_dy;
}

void ActeurMobile::setPeutBouger(bool peutBouger)
{
    m_peutBouger=peutBouger;
}

bool ActeurMobile::getPeutBouger() const
{
    return m_peutBouger;
}






void ActeurMobile::deplacement()
{
    ///On enregistre la position de l'acteur
    m_mx=m_x;
    m_my=m_y;

    ///On check si les coordonnées de l'acteur ne depasseront pas de la matrice
    //Pour les X
    if((((m_dx<=0)&&(m_x+m_dx>=0)) || ((m_dx>=0)&&(m_x+m_dx<=X_MAX-1))))
    {
        m_x+=m_dx;
    }
    else
    {
        m_dx=-m_dx; //sinon on rebondit

        //la balle doit s'actualiser mais pas les autres acteurs mobiles
        if(m_type=='B')
            m_x+=m_dx;
    }

    //Pour les Y
    if(((m_dy<=0)&&(m_y+m_dy>=0)) || ((m_dy>=0)&&(m_y+m_dy<=Y_MAX-1)))
    {
        m_y+=m_dy;
    }
    else
    {
        m_dy=-m_dy;

        if(m_type=='B')
            m_y+=m_dy;
    }
}

void ActeurMobile::stop()
{
    setDepX(0);
    setDepY(0);
}

void ActeurMobile::effaceTab(std::vector< std::vector <char> >& tab)
{
    tab[m_mx][m_my]=' ';
}

bool ActeurMobile::collision(Acteur const& cible)
{
    ///Si les deux acteurs sont aux memes coordonnées
    if(getX()==cible.getX() && getY()==cible.getY())
        return true;
    else
        return false;
}

bool ActeurMobile::blocPeutBouger(int direction, std::vector< std::vector <char> >const& tab)
{
    switch(direction)
    {
    case 1: //Gauche
        if((m_x-1)<0) return false;
        //on regarde dans la matrice si il n'y a pas d'acteur
        if(tab[m_x-1][m_y]=='C' || tab[m_x-1][m_y]=='T' || tab[m_x-1][m_y]=='P' ||
                tab[m_x-1][m_y]=='O' || tab[m_x-1][m_y]=='B')
        {
            return false;
        }
        else return true;

    case 2: //Droite
        if((m_x+1)>=X_MAX) return false;
        if(tab[m_x+1][m_y]=='C' || tab[m_x+1][m_y]=='T' || tab[m_x+1][m_y]=='P' ||
                tab[m_x+1][m_y]=='O' || tab[m_x+1][m_y]=='B')
        {
            return false;
        }
        else return true;

    case 3: //Haut
        if(tab[m_x][m_y-1]=='C' || tab[m_x][m_y-1]=='T' || tab[m_x][m_y-1]=='P' ||
                tab[m_x][m_y-1]=='O' || tab[m_x][m_y-1]=='B' || (m_y-1)<0)
        {
            return false;
        }
        else return true;

    case 4: //Bas
        if(tab[m_x][m_y+1]=='C' || tab[m_x][m_y+1]=='T' || tab[m_x][m_y+1]=='P' ||
                tab[m_x][m_y+1]=='O' || tab[m_x][m_y+1]=='B' || (m_y+1)>=Y_MAX)
        {
            return false;
        }
        else return true;

    default :
        return false;
    }
}
