#include "snoopy.h"

Snoopy::Snoopy()
{
    m_type='S';
    m_x=X_MAX/2;
    m_y=Y_MAX/2;
    m_dx=0;
    m_dy=0;
    m_alive=true;
}

void Snoopy::gauche()
{
    m_dx=-1;
    deplacement();
    m_dx=0;
}
void Snoopy::droite()
{
    m_dx=1;
    deplacement();
    m_dx=0;
}
void Snoopy::haut()
{
    m_dy=-1;
    deplacement();
    m_dy=0;
}
void Snoopy::bas()
{
    m_dy=1;
    deplacement();
    m_dy=0;
}

void Snoopy::clavier(bool& quit, Console* pconsole)
{
    if (pconsole->isKeyboardPressed())
    {
        char entree=pconsole->getInputKey();

        switch(entree)
        {
            //q -> quitter
            case 'q' : quit=true;
            break;

            case '4' : gauche();
            break;

            case '6' : droite();
            break;

            case '2' : bas();
            break;

            case '8' : haut();
            break;
        }
    }
}
