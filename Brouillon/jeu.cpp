#include "jeu.h"

void BoucleJeu(Console* pConsole)
{
    bool quitter=false;
    int nblocsP=alea(0,5), nblocsC=alea(0,15);

    ///DECLARATION & INITIALISATIONS
    //Acteurs fixes :
    std::vector<Acteur> oiseaux(4);
    initOiseaux(oiseaux);
    std::vector<Acteur> blocsP(nblocsP);
    initBlocsP(blocsP);
    std::vector<Acteur> blocsC(nblocsC);
    initBlocsC(blocsC);
    //Acteurs mobiles :

    //Snoopy :
    Snoopy player;
    //Matrice de jeu:
    Matrice jeu;
    initMatrice(jeu,oiseaux,blocsC,blocsP);


    ///BOUCLE DE JEU
    while(quitter!=true)
    {
        pConsole->clearConsole();
        player.clavier(quitter,pConsole);

        player.clavier(quitter,pConsole);
        player.deplacement();

        //clavier
        //actualiser
        //afficher
    }
}
