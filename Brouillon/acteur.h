#ifndef ACTEUR_H_INCLUDED
#define ACTEUR_H_INCLUDED

#include <iostream>

#include "console.h"


class Acteur
{

//modifi�/lu par acteur_mobile
protected:
    int m_x, m_y;
    char m_type;
    bool m_alive;

public:
    //Constructeurs-Destructeurs
    Acteur();
    Acteur(Acteur const& copie);
    Acteur(char type, int x, int y, bool alive=true);
    ~Acteur();

    //setters
    void setCoord(int x, int y);
    void setAlive(bool alive);
    void setType(char type);

    //Methodes
    void affichage(Console* pConsole);
};
#endif // ACTEUR_H_INCLUDED

/*

    bool getLife() const;
    int get_x() const;
    int get_y() const;
    char getType() const;

    */
