#ifndef ACTEUR_MOBILE_H_INCLUDED
#define ACTEUR_MOBILE_H_INCLUDED

#include <iostream>
#include "acteur.h"

class ActeurMobile : public Acteur
{
protected:
    int m_dx, m_dy;

public:
    ActeurMobile();
    ActeurMobile(char type, int x, int y, int dx=0, int dy=0);
    ~ActeurMobile();

    void setDepX(int dx);
    void setDepY(int dy);

    void deplacement();
};


#endif // ACTEUR_MOBILE_H_INCLUDED
