#include "acteur.h"

Acteur::Acteur()
{
    m_alive=true;
    m_x=0;
    m_y=0;
    m_type='0';
}

Acteur::Acteur(Acteur const& copie)
{
    m_alive=copie.m_alive;
    m_x=copie.m_x;
    m_y=copie.m_y;
    m_type=copie.m_type;
}

Acteur::Acteur(char type, int x, int y, bool alive)
{
    m_alive=alive;
    m_x=x;
    m_y=y;
    m_type=type;
}

Acteur::~Acteur()
{
    //pas de pointeur ici
}

void Acteur::setCoord(int x, int y)
{
    if(x>=0 && y>=0)
    {
        m_x=x;
        m_y=y;
    }
    else
        std::cout<<"Erreur !";
}

void Acteur::setAlive(bool alive)
{
    m_alive=alive;
}

void Acteur::setType(char type)
{
    m_type=type;
}


void Acteur::affichage(Console* pConsole)
{
    if(m_alive==true)
    {
        pConsole->gotoLigCol(m_x,m_y);
        std::cout<<m_type;
    }
}

