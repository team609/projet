#ifndef SNOOPY_H_INCLUDED
#define SNOOPY_H_INCLUDED

#include <iostream>
#include <cstdlib>

#include "acteur_mobile.h"
#include "console.h"

class Snoopy : public ActeurMobile
{
public :
    Snoopy();
    void clavier(bool& quit, Console* pConsole);
    void gauche();
    void droite();
    void haut();
    void bas();
};

#endif // SNOOPY_H_INCLUDED
