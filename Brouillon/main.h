#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include <iostream>
#include <cstdlib>
#include <time.h>
#include <string>

#include "console.h"
#include "jeu.h"

//saisie blind�e d'un entier entre les bornes inf et sup
int saisie_int(int nombre, int inf, int sup);

//retourne un nb aleatoire entre inf et supp
int alea(int inf, int supp);



#endif // MAIN_H_INCLUDED
