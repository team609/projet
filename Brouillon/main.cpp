#include "main.h"

int main()
{
    srand(time(NULL));

    Console* pConsole = NULL;
    pConsole = Console::getInstance();

    //on appelle jeu direct en attendant le menu
    BoucleJeu(pConsole);

    return 0;
}

int saisie_int(int nombre, int inf, int sup)
{
    std::string temp;
    bool test=false;

    do
    {
        std::getline(std::cin,temp,'\n');
        for(unsigned int i=0; i<temp.size(); i++)
        {
            test=true;
            if((temp[i]<'0')||(temp[i]>'9'))
            {
                test=false;
            }
        }
        nombre=atoi(temp.c_str());
        if((nombre<inf)||(nombre>sup)||(test==false))
        {
            test=false;
            std::cout<<"Erreur de saisie, Recommencez : "<<std::endl;
        }
    }
    while(test!=true);

    return nombre;
}

int alea(int inf, int supp)
{
    return (rand()%(supp+1)-inf);
}
