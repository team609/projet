#include "acteur_mobile.h"

ActeurMobile::ActeurMobile()
{
    m_x=0;
    m_y=0;
    m_dx=0;
    m_dy=0;
    m_type='0';
}

ActeurMobile::ActeurMobile(char type, int x, int y, int dx, int dy)
{
    m_x=x;
    m_y=y;
    m_dx=dx;
    m_dy=dy;
    m_type=type;
}

ActeurMobile::~ActeurMobile()
{

}

void ActeurMobile::setDepX(int dx)
{
    m_dx=dx;
}

void ActeurMobile::setDepY(int dy)
{
    m_dy=dy;
}

void ActeurMobile::deplacement()
{
    if((((m_dx<=0)&&(m_x+m_dx>=0))||((m_dx>=0)&&(m_x+m_dx<=X_MAX)))&&(m_dy==0))
    {
        m_x+=m_dx;
    }
    else
        m_dx=-m_dx;

    if((((m_dy<=0)&&(m_y+m_dy>=0))||((m_dy>=0)&&(m_y+m_dy<=Y_MAX)))&&(m_dx==0))
    {
        m_y+=m_dy;
    }
    else
        m_dy=-m_dy;
}

