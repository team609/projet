#ifndef FONCTIONS_H_INCLUDED
#define FONCTIONS_H_INCLUDED

#include <iostream>
#include <cstdlib>
#include <vector>

#include "console.h"
#include "snoopy.h"
#include "matrice.h"

void initOiseaux(std::vector <Acteur>& oiseaux);
void initTabActeurs(std::vector<Acteur>& tab,int x, int y, int nb, char type);
void initBlocsP(std::vector<Acteur>& blocs);
void initBlocsC(std::vector<Acteur>& blocs);
void initMatrice(Matrice& matrice, std::vector<Acteur> oiseaux, std::vector<Acteur> blocsC, std::vector<Acteur> blocsP);

void setCoordonnees(int& x, int& y);


/*
void affichageActeursFix(Console* pConsole, Acteur* oiseaux, Acteur* blocs1,int nb1, Acteur* blocs2, int nb2);
void affichageActeursMouv(Console* pConsole, Snoopy snoopy);*/

#endif // FONCTIONS_H_INCLUDED
