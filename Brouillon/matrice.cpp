#include "matrice.h"

Matrice::Matrice()
    :m_tab(0), m_blocsC(0), m_blocsP(0), m_player()
{

}

void Matrice::setBlocsC(std::vector <Acteur> blocs)
{
    m_blocsC=blocs;
}

void Matrice::setBlocsP(std::vector <Acteur> blocs)
{
    m_blocsP=blocs;
}

void Matrice::setOiseaux(std::vector <Acteur> oiseaux)
{
    m_oiseaux=oiseaux;
}


void Matrice::afficher(Console* pConsole)
{
    for(int i=0; i<Y_MAX; i++)
    {
        for(int j=0; j<X_MAX; j++)
        {
            std::cout<<m_tab[i][j];
        }
        std::cout<<std::endl;
    }
}
