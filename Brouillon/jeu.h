#ifndef JEU_H_INCLUDED
#define JEU_H_INCLUDED

#include <iostream>
#include <vector>

#include "main.h"
#include "matrice.h"
#include "console.h"
#include "snoopy.h"
#include "fonctions.h"


//contient la boucle de jeu
void BoucleJeu(Console* pConsole);

#endif // JEU_H_INCLUDED
