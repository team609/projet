#include "fonctions.h"

void initMatrice(Matrice& matrice, std::vector<Acteur> oiseaux, std::vector<Acteur> blocsC, std::vector<Acteur> blocsP)
{
    matrice.setOiseaux(oiseaux);
    matrice.setBlocsC(blocsC);
    matrice.setBlocsP(blocsP);
}

void initOiseaux(std::vector<Acteur>& oiseaux)
{
    oiseaux[0].setCoord(0,0);
    oiseaux[1].setCoord(X_MAX,0);
    oiseaux[2].setCoord(0,Y_MAX);
    oiseaux[3].setCoord(X_MAX,Y_MAX);
    for(int i=0; i<4; i++)
    {
        oiseaux[i].setType('O');
    }
}

void initBlocsP(std::vector<Acteur>& blocs)
{
    int x=0, y=0;

    initTabActeurs(blocs,x,y,blocs.size(),'T');

}

void initBlocsC(std::vector<Acteur>& blocs)
{
    int x=0, y=0;

    initTabActeurs(blocs,x,y,blocs.size(),'C');
}

void initTabActeurs(std::vector<Acteur>& tab,int x, int y, int nb, char type)
{
    for(int i=0; i<nb; i++)
    {
        setCoordonnees(x,y);
        tab[i].setType(type);
        tab[i].setCoord(x,y);
    }
}




void affichageActeursFix(Console* pConsole, Acteur* oiseaux, Acteur* blocs1,int nb1, Acteur* blocs2, int nb2)
{
    int i=0;

    for(i=0; i<4; i++)
    {
        oiseaux[i].affichage(pConsole);
    }

    for(i=0; i<nb1; i++)
    {
        blocs1[i].affichage(pConsole);
    }

    for(i=0;i<nb2;i++)
    {
        blocs2[i].affichage(pConsole);
    }
}

void affichageActeursMouv(Console* pConsole, Snoopy snoopy)
{
    snoopy.affichage(pConsole);
}
