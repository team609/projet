#ifndef MATRICE_H_INCLUDED
#define MATRICE_H_INCLUDED

#include <iostream>
#include <vector>

#include "console.h"
#include "snoopy.h"

class Matrice
{
private:

    //tableau 2 dimensions
    std::vector< std::vector <char> > m_tab;

    //joueur type Snoopy
    Snoopy m_player;

    //Tous les tableaux des autres acteurs (une dimension) :
    std::vector <Acteur> m_blocsP;
    std::vector <Acteur> m_blocsC;
    std::vector <Acteur> m_oiseaux;

public :

    Matrice();
    Matrice(std::vector <Acteur> blocsP, std::vector <Acteur> blocsC, std::vector <Acteur> oiseaux);

    void setBlocsP(std::vector <Acteur> blocs);
    void setBlocsC(std::vector <Acteur> blocs);
    void setOiseaux(std::vector <Acteur> oiseaux);

    void actualiser();
    void afficher(Console* pConsole);

};

#endif // MATRICE_H_INCLUDED
