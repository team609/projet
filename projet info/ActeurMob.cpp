#include "ActeurMob.h"


ActeurMobile::ActeurMobile()
{
    m_x=0;
    m_y=0;
    m_mx=m_x;
    m_my=m_y;
    m_dx=0;
    m_dy=0;
    m_type='0';
}

ActeurMobile::ActeurMobile(char type, int x, int y, int dx, int dy)
{
    m_x=x;
    m_y=y;
    m_mx=m_x;
    m_my=m_y;
    m_dx=dx;
    m_dy=dy;
    m_type=type;
}

ActeurMobile::~ActeurMobile()
{

}

int ActeurMobile::getDepX()
{
    return m_dx;
}

int ActeurMobile::getDepY()
{
    return m_dy;
}

void ActeurMobile::setDepX(int dx)
{
    m_dx=dx;
}

void ActeurMobile::setDepY(int dy)
{
    m_dy=dy;
}

void ActeurMobile::deplacement()
{
    ///On enregistre la position de l'acteur
    m_mx=m_x;
    m_my=m_y;

    ///si les coordonnées de l'acteur ne depassent(/depasseront) pas

    //Pour les X
    if((((m_dx<=0)&&(m_x+m_dx>=0)) || ((m_dx>=0)&&(m_x+m_dx<=X_MAX-1))))
    {
        m_x+=m_dx;
    }
    else
    {
        m_dx=-m_dx; //sinon on rebondit
        if(m_type=='B')
           m_x+=m_dx;
    }

    //Pour les Y
    if(((m_dy<=0)&&(m_y+m_dy>=0)) || ((m_dy>=0)&&(m_y+m_dy<=Y_MAX-1)))
    {
        m_y+=m_dy;
    }
    else
    {
        m_dy=-m_dy;
        if(m_type=='B')
            m_y+=m_dy;
    }
}

void ActeurMobile::stop()
{
    setDepX(0);
    setDepY(0);
}

void ActeurMobile::effaceTab(std::vector< std::vector <char> >& tab)
{
    tab[m_mx][m_my]=' ';
}

bool ActeurMobile::collision(Acteur const& cible)
{
    if(getX()==cible.getX() && getY()==cible.getY())
        return true;
    else
        return false;
}
