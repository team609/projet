#include <string>
#include <fstream>
#include <sstream>

#include "Jeu.h"

void affichageMenu(Console* pConsole);



void jeu()
{
    srand(time(NULL));

    Console* pConsole = NULL;
    pConsole = Console::getInstance();


    menu(pConsole);

    system("color F");
    pConsole->clearConsole();

    Console::deleteInstance();

}

void menu(Console* pConsole)
{
    int choix=0, niveau=0, lig=16;
    bool quit=false;
    std::string mdp, userName;

    system("mode con LINES=35 COLS=110");
    Niveau level(1);
    Niveau lvl2(2);
    Niveau lvl3(3);

    pConsole->clearConsole();
    while (!quit)
    {
        pConsole->clearConsole();

        Niveau level(1);
        Niveau lvl2(2);
        Niveau lvl3(3);

        choix=0;

        affichageMenu(pConsole);

        //while(!choix)
        {
            /* pConsole->gotoLigCol(1,lig);
             if(pConsole->isKeyboardPressed())
             {
                 char key=pConsole->getInputKey();
                 switch(key)
                 {
                 case 'i':
                     if(lig>16)
                     {
                         lig-=2;
                     }
                     break;

                 case 'k':
                     if(lig<26)
                     {
                         lig+=2;
                     }
                     break;

                 case ' ':
                     choix=(lig-14)/2;
                     break;

                 }
             }*/
            choix=saisie_int(choix,1,6);

            switch(choix)
            {
            case 1:
                pConsole->clearConsole();
                level.boucle(pConsole);
                break;

            case 2:
                pConsole->clearConsole();
                std::cout << "Entrez votre pseudo" << std::endl;
                std::cin >> userName;

                ///Ouverture fichier
                level=chargerPartie(userName);

                //std::cout<<level.getDifficulte();
                pConsole->pause();

                if(level.getDifficulte()!=0)
                {
                    std::cout<<"oui";
                    level.boucle(pConsole);

                }
                else
                {
                    pConsole->clearConsole();
                    std::cout<<"Pseudo inconnu !";

                }
                break;

            case 3:
                pConsole->clearConsole();
                std::cout << std::endl << std::endl << std::endl << std::endl << std::endl << "                             Entrez un Mot de Passe : " << std::endl << std::endl << std::endl;
                std::cout << "                               ";
                std::cin >> mdp;

                /// Selon les correspondances du mot de passe entr�, on retourne le numero du niveau appropri�

                if(mdp=="a")
                {
                    pConsole->clearConsole();
                    level.boucle(pConsole);
                }
                else if(mdp=="b")
                {
                    pConsole->clearConsole();
                    lvl2.boucle(pConsole);
                }
                else if(mdp=="c")
                {
                    pConsole->clearConsole();
                    lvl3.boucle(pConsole);
                }
                else
                {
                    pConsole->clearConsole();
                    std::cout << std::endl << std::endl << "                             Desole, mauvais mot de passe, vous allez etre redirige vers le menu" << std::endl << std::endl << std:: endl;
                    std::cout << "                             ";
                    system("pause");
                }

                break;

            case 4:
                pConsole->clearConsole();
                std::cout << std::endl << "                      COMMENT JOUER" << std::endl << std::endl;
                std::cout << "  Pour faire se deplacer Snoopy, utilisez les touches 'I', 'J', 'K' et 'L' pour Le faire bouger en haut," << std::endl << "  a gauche, en bas et a droite." << std::endl << std::endl;
                std::cout << "  Appuyez sur la touche 'A' pour casser un bloc de type 'C'" << std::endl << "  Marchez contre un bloc de type 'P' pour le pousser(vous ne pouvez que le pousser une fois)" << std::endl << "  Evitez les blocs de type 'T' et les balles 'B', faites nous confiance, on sait de quoi on parle" << std::endl << std::endl;
                std::cout << "  Rammasez les quatres Oiseaux 'O' au coins de chaque niveau dans le temps imparti pour gagner" << std::endl << std::endl;
                std::cout << "  Sauvegardez votre partie a tout moment en appuyant sur la touche 'S'" << std:: endl << std::endl;
                std::cout << "                                                                  ����        " << std::endl;
                std::cout << "                                                                  ��          " << std::endl;
                std::cout << "               Bonne chance ;)                                    ��    ��    " << std::endl;
                std::cout << "                                                                ����  ����    " << std::endl;
                std::cout << "                                  -Woodstock              ������              " << std::endl;
                std::cout << "                                                        ��                    " << std::endl;
                std::cout << "                                                      ��      ��      ����    " << std::endl;
                std::cout << "                                                      ��                ����  " << std::endl;
                std::cout << "                                                        ������        ��      " << std::endl;
                std::cout << "                                                              ��  ������      " << std::endl;
                std::cout << "                                                              ��  ��  ��      " << std::endl;
                std::cout << "                                                              ��  ��  ������  " << std::endl;
                std::cout << "                                                                ��  ��    ��  " << std::endl;
                std::cout << "                                                                  ��      ��  " << std::endl;
                std::cout << "                                                                    ����  ��  " << std::endl;
                std::cout << "                                                                      ������  " << std::endl;

                pConsole->pause();
                break;

            case 5:
                pConsole->clearConsole();
                afficherScores();
                break;

            case 6:

                quit=true;
                break;
            }
        }
    }
}

void afficherScores()
{
    system("cls");

    int rang=1;
    std::ifstream fichier("fichiers/scores.txt");
    std::string ligne;

    if(fichier)
    {
        std::cout << "                                             �     �     �" << std::endl;
        std::cout << "                                             ��   � �   ��" << std::endl;
        std::cout << "                                             � � �   � � �" << std::endl;
        std::cout << "                                             �  �     �  �" << std::endl;
        std::cout << "                                             �           �" << std::endl;
        std::cout << "                                             �������������" << std::endl << std::endl;
        std::cout << "                                             !HIGH SCORES!" << std::endl << std::endl << std::endl;
        while(getline(fichier, ligne))
        {

            std::cout << "      No." << rang << " " << ligne << " Points!!!" << std::endl << std::endl;
            rang++;
        }
        std::cout << std::endl << std::endl;
        system("pause");

    }
    else
    {
        std::cout << "Desole, nous n'avons pas trouve le fichier de sauvegarde" << std::endl;
    }

}


Niveau chargerPartie(std::string userName)
{
    std::string temp;

    std::string path="fichiers/";

    path+=userName;

    path+=".txt";

    std::ifstream fic(path.c_str());

    ActeurMobile snoopy('B',0,0);
    std::vector <ActeurMobile> balles;
    std::vector <Acteur> blocsC;
    std::vector <Acteur> blocsP;
    std::vector <Acteur> blocsT;
    std::vector <Acteur> oiseaux;
    int score=0;
    int niveau=0;
    int vies=0;
    int temps=0;

    int x=0, y=0;
    int dx=0, dy=0;
    bool decalage=false;



    if(fic)
    {

        while(std::getline(fic,temp))
        {
            //std::cout<<temp<<std::endl;

            if(temp[3]!=' ')
            {
                x=10*conv(temp[2])+conv(temp[3]);
                y=conv(temp[5]);

                decalage=true;
            }
            else
            {
                x=conv(temp[2]);
                y=conv(temp[4]);

                decalage=false;
            }
            ///On test le 1er char de la ligne -> type acteur
            if(temp[0]=='S')
            {
                //les 3eme et 5eme char sont les coord (� convertir en int)
                snoopy.setCoord(x,y);
            }

            if(temp[0]=='B')
            {

                if(!decalage)
                {
                    if(temp[6]!='-')
                    {
                        dx= conv(temp[6]);
                        if(temp[8]!='-')
                            dy=conv(temp[8]);
                        else
                            dy=-conv(temp[9]);
                    }
                    else
                    {
                        dx=-conv(temp[7]);
                        if(temp[9]!='-')
                            dy=conv(temp[9]);
                        else
                            dy=-conv(temp[10]);
                    }
                }
                else
                {
                    if(temp[7]!='-')
                    {
                        dx= conv(temp[7]);
                        if(temp[9]!='-')
                            dy=conv(temp[9]);
                        else
                            dy=-conv(temp[10]);
                    }
                    else
                    {
                        dx=-conv(temp[8]);
                        if(temp[10]!='-')
                            dy=conv(temp[10]);
                        else
                            dy=-conv(temp[11]);
                    }
                }
                ActeurMobile balle('B',x,y,dx,dy);
                balles.push_back(balle);
            }
            if(temp[0]=='C')
            {
                Acteur blocC('C',x,y);
                blocsC.push_back(blocC);
            }
            if(temp[0]=='P')
            {
                Acteur blocP('P',x,y);
                blocsP.push_back(blocP);
            }
            if(temp[0]=='T')
            {
                Acteur blocT('T',x,y);
                blocsC.push_back(blocT);
            }
            if(temp[0]=='O')
            {
                Acteur oiseau('O',x,y);
                oiseaux.push_back(oiseau);
            }

            if(temp[0]=='s')
            {
                for(int i=0; i<temp.size(); i++)
                {
                    score+=(pow(10,temp.size()-i-1))*(conv(temp[i]));
                }
            }
            if(temp[0]=='n')
            {
                niveau=conv(temp[2]);
            }
            if(temp[0]=='v')
            {
                vies=conv(temp[2]);
            }
            if(temp[0]=='t')
            {
                for(int i=0; i<temp.size(); i++)
                {
                    temps+=(pow(10,temp.size()-i-1))*(conv(temp[i]));
                }
            }
        }
        ///On cree un niveau possedant toutes les caracteristiques
        std::vector < std::vector<char> > tab;
        for(int i=0; i<X_MAX; i++)
        {
            tab.push_back(std::vector<char> (Y_MAX, ' '));
        }

        Niveau level(niveau, score,userName, vies, temps, snoopy, balles, blocsC, blocsP, blocsT, oiseaux, tab);
        std::cout<<"Partie chargee avec succes, Niveau : "<<level.getDifficulte()<<" vies :"<<level.getnbvies()<<std::endl;

        return level;
    }
    else
    {
        std::cout<<"Impossible d'ouvrir le fichier!";
        Niveau level(0);
        return level;
    }
/*

    std::cout<<"Snoopy x : "<<snoopy.getX()<<" / y : "<<snoopy.getY()<<std::endl;
    for(int i=0; i<balles.size(); i++)
    {
        std::cout<<"Balle x : "<<balles[i].getX()<<" / y : "<<balles[i].getY()<<" / dx : "<<balles[i].getDepX()<<" / dy : "<<balles[i].getDepY()<<std::endl;
    }
    for(int i=0; i<oiseaux.size(); i++)
    {
        std::cout<<"Oiseau x : "<<oiseaux[i].getX()<<" / y : "<<oiseaux[i].getY()<<std::endl;
    }

    for(int i=0; i<blocsC.size(); i++)
    {
        std::cout<<"BlocsC x : "<<blocsC[i].getX()<<" / y : "<<blocsC[i].getY()<<std::endl;
    }


    std::cout<<"Score : "<<score<<std::endl;
    std::cout<<"Niveau : "<<niveau<<std::endl;
    std::cout<<"Vies : "<<vies<<std::endl;
    std::cout<<"Temps : "<<temps<<std::endl;

    std::vector < std::vector <char> > tab;
    for(int i=0; i<X_MAX; i ++)
    {
        tab.push_back(std::vector<char> (Y_MAX,' '));
    }
*/

}
//convertit un char en chiffre
int conv(char caract)
{
    if(caract=='0')
        return 0;
    if(caract=='1')
        return 1;
    if(caract=='2')
        return 2;
    if(caract=='3')
        return 3;
    if(caract=='4')
        return 4;
    if(caract=='5')
        return 5;
    if(caract=='6')
        return 6;
    if(caract=='7')
        return 7;
    if(caract=='8')
        return 8;
    if(caract=='9')
        return 9;
    return 0;
}

void affichageMenu(Console* pConsole)
{
    pConsole->gotoLigCol(0,0);

    system("color 70");
    std::cout << std::endl;
    std::cout << " �     ��     ���   ���� �   �  ��  �   �  ��  �  � ����         ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������������";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "                      " << std::endl;
    std::cout << " �    �  �    �  �  �    �   � �  � ��  � �  � �  � �          ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(WHITE,WHITE);
    std::cout << "            ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������������";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "          " << std::endl;
    std::cout << " �    ����    ���   ���  �   � ���� � � � �    ���� ���      ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK, WHITE);
    std::cout << "                          ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "      " << std::endl;
    std::cout << " �    �  �    �  �  �     � �  �  � �  �� �  � �  � �      ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "    " << std::endl;
    std::cout << " ���� �  �    �  �  ����   �   �  � �   �  ��  �  � ����   ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����" << std::endl;
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "                                                           ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����" << std::endl;
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << " ���  ����    ��� �   �  ��   ��  ���  �   �             ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                                    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "  " << std::endl;
    std::cout << " �  � �      �    ��  � �  � �  � �  �  � �              ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                              ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "  " << std::endl;
    std::cout << " �  � ���     ��  � � � �  � �  � ���    �               ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��������";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                        ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "    " << std::endl;
    std::cout << " �  � �         � �  �� �  � �  � �     �                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "      " << std::endl;
    std::cout << " ���  ����   ���  �   �  ��   ��  �    �                 ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "      ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "          ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "        " << std::endl;
    std::cout << "                                                         ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��������";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "        ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����������";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "            " << std::endl;
    std::cout << "                                                           ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "                  " << std::endl;
    std::cout << "                                                           ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "        ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "        ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "    " << std::endl;
    std::cout << "                                                             ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��������";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "  ";
    pConsole->_setColor(RED,RED);
    std::cout << "RRRR";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "  " << std::endl;
    std::cout << " 1. Nouveau Jeu                  Utilisez les touches                    ";
    pConsole->_setColor(RED,RED);
    std::cout << "RRRR";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "        ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "    " << std::endl;
    std::cout << "                                                                         ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "              ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "    " << std::endl;
    std::cout << " 2. Charger Partie               'i' et 'k' pour naviguer                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "          ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "          " << std::endl;
    std::cout << "                                                                         ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "      ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "              " << std::endl;
    std::cout << " 3. Entrer Un Mot De Passe       en haut et en bas                     ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "            " << std::endl;
    std::cout << "                                                                       ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "            ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "            " << std::endl;
    std::cout << " 4. Comment Jouer                et appuyez sur la barre               ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "            ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "            " << std::endl;
    std::cout << "                                                                       ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "            ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "            " << std::endl;
    std::cout << " 5. Scores                       d'espace pour valider                 ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "            ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "            " << std::endl;
    std::cout << "                                                                     ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "        ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "              " << std::endl;
    std::cout << " 6. Quitter                      votre choix                     ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "    ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "                " << std::endl;
    std::cout << "                                                                   ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����������";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "      ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "              " << std::endl;
    std::cout << "                                                                       ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "        ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "����";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "          " << std::endl;
    std::cout << "                                                                       ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "        " << std::endl;
    std::cout << "                                                                     ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,WHITE);
    std::cout << "                  ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "��";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "        " << std::endl;
    std::cout << "                                                                       ";
    pConsole->_setColor(BLACK,BLACK);
    std::cout << "������������������";
    pConsole->_setColor(BLACK,DEFAULT);
    std::cout << "          " << std::endl;
    std::cout << " Copyright, TradeMark, All Rights Reserved, AG.JP.ES&Co." << std::endl << std::endl;

}
