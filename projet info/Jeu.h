#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

#include <math.h>
#include "console.h"
#include "Niveau.h"

void jeu();
void menu(Console* pconsole);
void afficherScores();
Niveau chargerPartie(std::string userName);
int conv(char caract);

#endif // MENU_H_INCLUDED
