#ifndef ACTEUR_MOBILE_H_INCLUDED
#define ACTEUR_MOBILE_H_INCLUDED

#include <iostream>
#include "Acteur.h"

class ActeurMobile : public Acteur
{
private:

    int m_dx, m_dy;
    int m_mx, m_my;
    bool m_peutBouger;


public:
    ActeurMobile();
    ActeurMobile(char type, int x, int y, int dx=0, int dy=0);
    ~ActeurMobile();

    void setDepX(int dx);
    void setDepY(int dy);
    void setPeutBouger(bool peutBouger) { m_peutBouger=peutBouger; }

    int getDepX();
    int getDepY();
    bool getPeutBouger() const { return m_peutBouger; }

    void stop();
    void deplacement();
    void effaceTab(std::vector< std::vector <char> >& tab);

    ///si l'acteur est en coolision avec la cible
    bool collision(Acteur const& cible);

    bool blocPeutBouger(int direction, std::vector< std::vector <char> >& tab);
};


#endif // ACTEUR_MOBILE_H_INCLUDED
