#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

#include <math.h>
#include "console.h"
#include "Niveau.h"

void jeu();
void menu(Console* pconsole);
void afficherScores();
Niveau chargerPartie(std::string userName, int& niv);
int conv(char caract);

void affichageMenu(Console* pConsole);
void regles();


#endif // MENU_H_INCLUDED
