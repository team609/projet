#ifndef PARTIE_H_INCLUDED
#define PARTIE_H_INCLUDED

#include "console.h"
#include "Niveau.h"

class Partie
{
private:

    Niveau m_lvl;
    int m_niveau;
    int m_score;
    bool m_fin;
    std::string m_pseudo;
    bool m_charge;

public:
    Partie();
    Partie(int niveau);
    ~Partie();

    void setCharge(bool charge) { m_charge=charge; }
    void setNiveau(int niveau);
    void setLvl(Niveau lvl) { if(lvl.getDifficulte()!=0) m_lvl=lvl; }
    int getNiveau() const { return m_niveau; }
    Niveau getLvl() const { return m_lvl; }

    void lancerPartie(Console* pConsole);
    void boucle(Console* pConsole);
    void sauvegardeScore();
};

#endif // PARTIE_H_INCLUDED
