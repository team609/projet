#include "partie.h"
#include <fstream>

Partie::Partie()
    :m_lvl(), m_niveau(0), m_score(0), m_fin(false), m_pseudo(""), m_charge(false)
{

}

Partie::~Partie()
{

}

void Partie::setNiveau(int niveau)
{
    if(niveau>0 && niveau<=15)
        m_niveau=niveau;
}

void Partie::lancerPartie(Console* pConsole)
{
    boucle(pConsole);
}

void Partie::boucle(Console* pConsole)
{
    m_niveau=1;

    while(!m_fin)
    {
        if(!m_charge)
            m_lvl.initialiser(6*m_niveau,3*m_niveau,6*(m_niveau-1)+1,m_niveau,m_niveau);
        /*std::cout<<"init effectuee"<<std::endl;
        pConsole->pause();*/
        m_lvl.boucle(pConsole);

        m_score+=m_lvl.getScore();

        pConsole->pause();

        std::cout<<"Que voulez vous faire?"<<std::endl;
        int choix=0;

        if((m_niveau<3)&&(m_lvl.getnboiseaux()==0))
        {


            m_lvl.finNiveau();
            m_lvl.libererActeur();

            std::cout<<"1->Recommencer niveau "<<m_niveau<<std::endl;
            std::cout<<"2->Passer au niveau "<<m_niveau +1 <<std::endl;
            std::cout<<"3->Sauvegarder et quitter "<<std::endl;

            choix=saisie_int(choix,1,3);

            switch(choix)
            {
            case 1 :
                pConsole->clearConsole();
                std::cout<<"On recommence !"<<std::endl;
                pConsole->pause();
                break;

            case 2 :
                pConsole->clearConsole();
                setNiveau(m_niveau+1);
                m_lvl.setDifficult(m_niveau);
                std::cout<<"Niveau : "<<m_niveau;
                pConsole->pause();
                break;

            case 3:
                pConsole->clearConsole();
                sauvegardeScore();
                pConsole->pause();
                m_fin=true;
                break;
            }
        }
        else
            {
                std::cout<<"1->Recommencer niveau "<<m_niveau<<std::endl;
                std::cout<<"2->Enregistrer score et quitter "<<std::endl;

                choix=saisie_int(choix,1,3);

                switch(choix)
                {
                case 1 :
                    pConsole->clearConsole();
                    std::cout<<"On recommence !"<<std::endl;
                    pConsole->pause();
                    break;

                case 2 :
                    pConsole->clearConsole();
                    sauvegardeScore();
                    pConsole->pause();
                    m_fin=true;
                    break;

                }

            }


    }
}


void Partie::sauvegardeScore()
{
    std::ofstream fichier("fichiers/scores.txt",std::ios::app);

    if(fichier)
    {
        while(m_pseudo=="")
        {
            std::cout<<"Saisir le pseudo pour la savegarde"<<std::endl;
            std::getline(std::cin,m_pseudo);
        }
        fichier<<m_pseudo<<" "<<m_score<<std::endl;
        std::cout<<"Sauvegarde effectuee avec succes !"<<std::endl;
    }
    else
    {
        std::cout<<"Ouverture du fichier impossible..."<<std::endl;
    }

    fichier.close();

}


